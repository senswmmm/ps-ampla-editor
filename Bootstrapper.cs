namespace PSAmplaEditor
{
    using Autofac;
    using Autofac.Builder;
    using Citect.Ampla.DependencyInjection;
    using Citect.Common.Reflection;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Windows.Forms;

    public abstract class Bootstrapper<TShell> where TShell: Form, IDisposable
    {
        protected Bootstrapper()
        {
        }

        protected virtual void ConfigureContainer(ContainerBuilder builder)
        {
            Assembly[] assemblies = this.GetModuleAssemblies().ToArray<Assembly>();
            builder.RegisterAssemblyModules<UIModule>(assemblies);
        }

        protected abstract TShell CreateShell();
        protected abstract ILifetimeScope GetLifetimeScope();

        public void Dispose()
        {
            if (this.Container != null)
            {
                this.Container.Dispose();
                this.Container = null;
            }
        }

        protected virtual IEnumerable<Assembly> GetModuleAssemblies()
        {
            return AppDomain.CurrentDomain.GetAssemblies().Where<Assembly>(new Func<Assembly, bool>(AssemblyLoader.IsAmplaAssembly));
        }


        public void Run(bool autoRun)
        {
            ContainerBuilder builder = new ContainerBuilder();
            this.ConfigureContainer(builder);
            this.Container = builder.Build(ContainerBuildOptions.Default);
            this.Shell = this.CreateShell();
            if (autoRun)
            {
                Application.Run(this.Shell);
            }
        }
        public void Run()
        {
            this.Run(true);
        }



        public IContainer Container { get; private set; }

        public TShell Shell { get; private set; }


    }
}
