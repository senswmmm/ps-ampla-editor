﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PSAmplaEditor
{
    class AmplaItem
    {
        #region fields---------------------------------------------------------------------------------------

        private Type type;
        private Guid id;
        private string fullname;

        #endregion //fields

        #region properties--------------------------------------------------------------------------------------

        /// <summary>
        /// Node fullname
        /// </summary>
        public Type ObjectType
        {
            set
            {
                type = value;
            }
            get
            {
                return type;
            }
        }

        /// <summary>
        /// Guid
        /// </summary>
        public Guid Id
        {
            set
            {
                id = value;
            }
            get
            {
                return id;
            }
        }

        /// <summary>
        /// Node fullname
        /// </summary>
        public string Fullname
        {
            set
            {
                fullname = value;
            }
            get
            {
                return fullname;
            }
        }
        #endregion //properties
    }

}
