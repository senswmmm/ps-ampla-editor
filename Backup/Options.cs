using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace PSAmplaEditor
{

    class Options
    {
        #region private properties -------------------------------------------------------------------------------------------------------
        private string m_nameFilter = string.Empty;
        private string m_typeFilter = string.Empty;
        private string m_pNameFilter = string.Empty;
        private string m_Filename = string.Empty;
        private Boolean m_includeItems;
        private Boolean m_defaults;
        private Boolean m_importAll;
        private Form1 m_mainForm;
        #endregion //private properties

        #region constructors -------------------------------------------------------------------------------------------------------

        public Options()
        {
        }
        public Options(Form1 mainform)
        {
            m_mainForm = mainform;
            m_nameFilter = mainform.NameFilter;
            m_pNameFilter = mainform.PNameFilter;
            m_typeFilter = mainform.TypeFilter;
            m_includeItems = mainform.IncludeItems;
            m_defaults = mainform.Defaults;
            m_importAll = mainform.ImportAll;
            m_Filename = mainform.Filename;
        }

        #endregion //constructors

        #region properties-----------------------------------------------------------------------------------------------------------

        public string NameFilter
        {
            get{return m_nameFilter;}
            set{m_nameFilter = value;}
        }

        public string TypeFilter
        {
            get { return m_typeFilter; }
            set { m_typeFilter = value; }
        }

        public string PNameFilter
        {
            get { return m_pNameFilter; }
            set { m_pNameFilter = value; }
        }

        public Boolean IncludeItems
        {
            get { return m_includeItems; }
            set { m_includeItems = value; }
        }

        public Boolean Defaults
        {
            get { return m_defaults; }
            set { m_defaults = value; }
        }

        public Boolean ImportAll
        {
            get { return m_importAll; }
            set { m_importAll = value; }
        }
        public string FileName
        {
            get {return m_Filename; }
            set { m_Filename = value; }
        }
        
        #endregion //properties

        #region methods-----------------------------------------------------------------------------------------------------------------------

        public void ShowMessage(string message)
        {
            MessageBox.Show(message);
        }

        public void UpdateStatus(string message)
        {
            m_mainForm.Invoke(new UpdateStatus(m_mainForm.UpdateStatusBar), new object[] { message });
        }

        #endregion //methods
    }



}
