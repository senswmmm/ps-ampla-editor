//-----------------------------------------------------------------------------------------------------
//  Copyright (c) 2006 Citect Pty Limited.
//  All rights reserved. 
//	
//	History:
//			23/05/2006		Julio Montufar		Original
//-----------------------------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace PSAmplaEditor
{
    public class Log
    {
		#region fields-----------------------------------------------------------------------------------

        private TextWriter mainLog;
        private string LogFile;
		private bool fileopen = false;

		#endregion //fields

		#region constructors-----------------------------------------------------------------------------

        public Log()
        {
            this.OpenLogFile();
        }

		#endregion //constructor

		#region methods----------------------------------------------------------------------------------

       /// <summary>
       /// Update status bar and append line to log file
       /// </summary>
       /// <param name="line"></param>
        public void WriteLine(string line)
        {
			if (!fileopen)
			{
				OpenLogFile();
			}
			mainLog.WriteLine(line);
        }

        /// <summary>
        /// Open log file
        /// </summary>
        public void OpenLogFile()
        {
            LogFile = string.Concat(GetAmplaDirectory(), "\\", Application.ProductName, ".log");
			if (File.Exists(LogFile))
			{
				mainLog = File.AppendText(LogFile);
			}
			else
			{
				mainLog = File.CreateText(LogFile);
			}
			fileopen = true;
        }

		/// <summary>
		/// Close log file
		/// </summary>
        public void CloseFile()
        {
            if (File.Exists(LogFile))
            {
				mainLog.Close();
				fileopen = false;
            }
        }

		/// <summary>
		/// Log exception
		/// </summary>
		/// <param name="ex"></param>
		public void RaiseException(Exception ex)
		{
			if (!fileopen)
			{
				OpenLogFile();
			}
			mainLog.WriteLine("==================================================");
            mainLog.WriteLine("Error Date/Time: " + DateTime.Now.ToString());
			mainLog.WriteLine("Error Message: "  + "\n" + ex.Message);
			mainLog.WriteLine("Error Source: "  + "\n" + ex.Source);
			mainLog.WriteLine("Inner Exception: " +  "\n" + ex.InnerException);
			mainLog.WriteLine("Stack Trace: " +  "\n" + ex.StackTrace);
		}

        /// <summary>
        /// Validate open excel file
        /// </summary>
        /// <returns>Success or failure</returns>
        private string GetAmplaDirectory()
        {
            string[] ld = Directory.GetLogicalDrives();
            for (int i = 0; i < ld.Length; i++)
            {
                string path = Path.Combine(ld[i], @"Program Files\Citect\Ampla");
                if (Directory.Exists(path))
                {
                    return path;
                }
                else
                {
                    continue;
                }
            }
            return string.Empty;
        }
		#endregion methods //methods
		
    }
}
