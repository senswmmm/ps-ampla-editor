//-----------------------------------------------------------------------------------------------
//  Copyright (c) 2006 Citect Pty Limited.
//  All rights reserved. 
//	
//	History:
//			10/06/2007		Julio Montufar		Original
//	Version 3.2XXX
//-----------------------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using System.Diagnostics;
using System.Management;
using System.Runtime.Remoting.Messaging;
using System.Timers;
using System.Configuration;
using System.Reflection;
using System.Security.Principal;
using System.Security;
using System.Security.Policy;
using System.Security.Permissions;

#region Citect/Ampla refereneces------------------------------------------------------------------

using Citect.Ampla.Framework;
using Citect.Ampla.Framework.ClassSystem;
using Citect.Ampla.Framework.Gui;
using Citect.Ampla.General.Server.Helper;
//using Citect.Ampla.Studio.SandDock.Gui;
using Citect.Common.Gui;
using Autofac.Core;
using Autofac;
using Autofac.Features.OwnedInstances;
//using Autofac;
//using Autofac.Features.OwnedInstances;
using Citect.Ampla.Data;
//using Citect.Ampla.Framework;
//using Citect.Ampla.Framework.Gui;
using Citect.Ampla.Runtime.Data;
using Citect.Ampla.Runtime.Remoting;
using Citect.Ampla.Runtime.Remoting.Scripting;
//using Citect.Ampla.Studio.SandDock.Gui.Properties;
using Citect.Common;
using Citect.Common.Globalization;
//using Citect.Common.Gui;
using Citect.Common.Gui.Hierarchical;
using Citect.Common.UpgradeServices;
using Citect.Security.Authorization;
//using System;
//using System.ComponentModel;
//using System.Diagnostics;
//using System.Drawing;
using System.Globalization;
//using System.IO;
using System.Net;
using System.Net.Sockets;
//using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
//using System.Text;
//using System.Threading;
//using System.Windows.Forms;
//using TD.SandDock;


#endregion

namespace PSAmplaEditor
{
	/// <summary>
	/// Ampla Editor Tool Main Form. This form enables the user make the required selection 
	/// for import or export functions required. It minimises error prone data entry
	/// </summary>
	/// 

	#region delegates-----------------------------------------------------------------------------

	delegate bool GetAmplaProject();
	delegate bool ExportProperties(bool recursive,List<AmplaItem> objectToProcess );
	delegate bool ImportProperties();
	delegate bool SaveAmplaProject();
	delegate bool PasteItem(IDataObject idataobject);
	delegate void AddNode(TreeNodeCollection nodes,ParentNode pn);
    delegate void AddClassNode(TreeNodeCollection nodes, ParentNode pn);
	delegate void Loading(bool load);
	delegate void SetOnTimer(bool load);
	delegate void ConcatenateText();
	delegate void UpdateStatus(string status);
	delegate void SetNotificationText(string text);
	delegate void ResetNotificationText();
	delegate void ResetCheckBoxesValue();
	delegate void FillTreeView();
    delegate void FillClassTreeView();
	delegate void SetButtonEnable(Button cmd, bool Value);
	delegate void SetMenuItemEnable(MenuItem mi, bool Value);
	delegate void SetLabelTextValue(Label lbl, string text);
    delegate void ShowMessageBox(IWin32Window owner, string text);

	#endregion //delegates

    public class AppForm : System.Windows.Forms.Form

	{
		#region windows form fields---------------------------------------------------------------
		private System.Windows.Forms.TreeView tvStudio;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button cmdOK;
		private System.Windows.Forms.Button cmdCancel;
		private System.Windows.Forms.Button cmdBrowser;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.RadioButton rbExport;
		private System.Windows.Forms.RadioButton rbImport;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Label lblProgress;
		private System.Windows.Forms.CheckBox cbRecursive;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblAmplaVersion;
		private System.Windows.Forms.TextBox txtFile;
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label lblProductVersion;
		private System.Windows.Forms.ListBox lbAmpla;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.Button cmdAdd;
		private System.Windows.Forms.Button cmdRemove;
		private System.Windows.Forms.Button cmdRemoveAll;
		private System.Windows.Forms.CheckBox cbImportAll;
		private System.Windows.Forms.Button cmdSaveProject;
		private System.Windows.Forms.Label lblProjectName;
		private System.Windows.Forms.Label label5;

		#endregion //forms fields
		
		#region fields----------------------------------------------------------------------------

		private ClientProject project;
		private System.Timers.Timer onTimer;
		private System.Timers.Timer offTimer;
		private int index = 0;
		private string text = "Loading Ampla Studio...................";
		private string fullname = string.Empty;
		private Log mainLog;
		private PolicyLevel policyUserLevel = null;
		private CodeGroup UserCodeGroupRoot = null;
		private UnionCodeGroup myCodeGroup = null;
		private bool projectloaded = false;
		private Process process;
		private TreeNode amplanode;
		private static DataFormats.Format clipboardFormat = DataFormats.GetFormat("AmplaStudio Format");
		private AmplaProject aproject;
		private ContextMenu cm;
        private Options options;
        private ILifetimeScope lifetimeScope;
        private Func<Uri, Project.ProgressUpdate, Owned<ClientProject>> clientProjectFactory;

		#endregion //fields

		#region constants-------------------------------------------------------------------------

		private const double ontime = 200;
		private System.Windows.Forms.Button cmdReload;
        private Label label4;
        private TextBox txtFilterByType;
        private TextBox txtFilterByName;
        private Label label6;
        private CheckBox cbDefaults;
        private CheckBox cbItems;
        private Label label7;
        private TextBox txtFilterByPName;
        private TreeView tvClass;
        private GroupBox groupBox6;
        private TextBox txtStatus;
		
		private const double offtime = 200;

		#endregion //constants

		#region events----------------------------------------------------------------------------

		public event TreeViewEventHandler NodeClick;

		#endregion //events

		#region constructors----------------------------------------------------------------------

		public AppForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}
        public AppForm(string[] commandLineArguments, ILifetimeScope lifetimeScope)
        {
            //this.commandLineArguments = commandLineArguments ?? new string[] { "" };
            //ArgumentCheck.IsNotNull(lifetimeScope, "lifetimeScope");
            //this.clientProjectFactory = lifetimeScope.Resolve<Func<Uri, Project.ProgressUpdate, Owned<ClientProject>>>();
            //this.lifetimeScope = lifetimeScope;
            InitializeComponent();
        }



		#endregion //constructors

		#region disposing methods-----------------------------------------------------------------

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#endregion // dispose

		#region Windows Form Designer generated code----------------------------------------------
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.tvStudio = new System.Windows.Forms.TreeView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lblProjectName = new System.Windows.Forms.Label();
            this.cmdOK = new System.Windows.Forms.Button();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.txtFile = new System.Windows.Forms.TextBox();
            this.cmdBrowser = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rbExport = new System.Windows.Forms.RadioButton();
            this.rbImport = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtFilterByPName = new System.Windows.Forms.TextBox();
            this.cbItems = new System.Windows.Forms.CheckBox();
            this.cbDefaults = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtFilterByType = new System.Windows.Forms.TextBox();
            this.txtFilterByName = new System.Windows.Forms.TextBox();
            this.cbRecursive = new System.Windows.Forms.CheckBox();
            this.cbImportAll = new System.Windows.Forms.CheckBox();
            this.lblProgress = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lblProductVersion = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblAmplaVersion = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbAmpla = new System.Windows.Forms.ListBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.cmdAdd = new System.Windows.Forms.Button();
            this.cmdRemove = new System.Windows.Forms.Button();
            this.cmdRemoveAll = new System.Windows.Forms.Button();
            this.cmdSaveProject = new System.Windows.Forms.Button();
            this.cmdReload = new System.Windows.Forms.Button();
            this.tvClass = new System.Windows.Forms.TreeView();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtStatus = new System.Windows.Forms.TextBox();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // tvStudio
            // 
            this.tvStudio.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tvStudio.FullRowSelect = true;
            this.tvStudio.LabelEdit = true;
            this.tvStudio.Location = new System.Drawing.Point(48, 40);
            this.tvStudio.Name = "tvStudio";
            this.tvStudio.Size = new System.Drawing.Size(320, 355);
            this.tvStudio.TabIndex = 0;
            this.tvStudio.AfterLabelEdit += new System.Windows.Forms.NodeLabelEditEventHandler(this.tvStudio_AfterLabelEdit);
            this.tvStudio.BeforeExpand += new System.Windows.Forms.TreeViewCancelEventHandler(this.tvStudio_BeforeExpand);
            this.tvStudio.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tvStudio_MouseUp);
            this.tvStudio.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvStudio_AfterSelect);
            this.tvStudio.Click += new System.EventHandler(this.tvStudio_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Location = new System.Drawing.Point(24, 16);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(368, 398);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Project Explorer";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.Location = new System.Drawing.Point(37, 240);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(160, 16);
            this.label5.TabIndex = 5;
            this.label5.Text = "Ampla Server  - PC Name:";
            // 
            // lblProjectName
            // 
            this.lblProjectName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblProjectName.BackColor = System.Drawing.SystemColors.Window;
            this.lblProjectName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblProjectName.Location = new System.Drawing.Point(200, 234);
            this.lblProjectName.Name = "lblProjectName";
            this.lblProjectName.Size = new System.Drawing.Size(144, 24);
            this.lblProjectName.TabIndex = 6;
            this.lblProjectName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmdOK
            // 
            this.cmdOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdOK.Location = new System.Drawing.Point(504, 710);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.Size = new System.Drawing.Size(144, 32);
            this.cmdOK.TabIndex = 3;
            this.cmdOK.Text = "&OK";
            this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
            // 
            // cmdCancel
            // 
            this.cmdCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdCancel.Location = new System.Drawing.Point(664, 710);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(144, 32);
            this.cmdCancel.TabIndex = 4;
            this.cmdCancel.Text = "&Cancel";
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // txtFile
            // 
            this.txtFile.Location = new System.Drawing.Point(65, 21);
            this.txtFile.Name = "txtFile";
            this.txtFile.Size = new System.Drawing.Size(199, 20);
            this.txtFile.TabIndex = 5;
            this.txtFile.TextChanged += new System.EventHandler(this.txtFile_TextChanged);
            // 
            // cmdBrowser
            // 
            this.cmdBrowser.Location = new System.Drawing.Point(279, 19);
            this.cmdBrowser.Name = "cmdBrowser";
            this.cmdBrowser.Size = new System.Drawing.Size(32, 24);
            this.cmdBrowser.TabIndex = 6;
            this.cmdBrowser.Text = "...";
            this.cmdBrowser.Click += new System.EventHandler(this.cmdBrowser_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(16, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 16);
            this.label1.TabIndex = 7;
            this.label1.Text = "File:";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.cmdBrowser);
            this.groupBox2.Controls.Add(this.txtFile);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(480, 574);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(352, 56);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Select Xml or Excel File";
            // 
            // rbExport
            // 
            this.rbExport.Checked = true;
            this.rbExport.Location = new System.Drawing.Point(24, 74);
            this.rbExport.Name = "rbExport";
            this.rbExport.Size = new System.Drawing.Size(215, 32);
            this.rbExport.TabIndex = 9;
            this.rbExport.TabStop = true;
            this.rbExport.Text = "Export Properties";
            this.rbExport.CheckedChanged += new System.EventHandler(this.rbExport_CheckedChanged);
            // 
            // rbImport
            // 
            this.rbImport.Location = new System.Drawing.Point(24, 18);
            this.rbImport.Name = "rbImport";
            this.rbImport.Size = new System.Drawing.Size(215, 32);
            this.rbImport.TabIndex = 10;
            this.rbImport.Text = "Import Properties";
            this.rbImport.CheckedChanged += new System.EventHandler(this.rbImport_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.txtFilterByPName);
            this.groupBox3.Controls.Add(this.cbItems);
            this.groupBox3.Controls.Add(this.cbDefaults);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.txtFilterByType);
            this.groupBox3.Controls.Add(this.txtFilterByName);
            this.groupBox3.Controls.Add(this.rbImport);
            this.groupBox3.Controls.Add(this.cbRecursive);
            this.groupBox3.Controls.Add(this.cbImportAll);
            this.groupBox3.Controls.Add(this.rbExport);
            this.groupBox3.Location = new System.Drawing.Point(480, 304);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(352, 264);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Import/Export Properties";
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(21, 224);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(120, 24);
            this.label7.TabIndex = 22;
            this.label7.Text = "Filter By property:";
            // 
            // txtFilterByPName
            // 
            this.txtFilterByPName.Location = new System.Drawing.Point(142, 224);
            this.txtFilterByPName.Name = "txtFilterByPName";
            this.txtFilterByPName.Size = new System.Drawing.Size(197, 20);
            this.txtFilterByPName.TabIndex = 21;
            this.txtFilterByPName.Text = "*";
            // 
            // cbItems
            // 
            this.cbItems.Checked = true;
            this.cbItems.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbItems.Location = new System.Drawing.Point(48, 133);
            this.cbItems.Name = "cbItems";
            this.cbItems.Size = new System.Drawing.Size(161, 24);
            this.cbItems.TabIndex = 20;
            this.cbItems.Text = "Include Items";
            // 
            // cbDefaults
            // 
            this.cbDefaults.Location = new System.Drawing.Point(198, 103);
            this.cbDefaults.Name = "cbDefaults";
            this.cbDefaults.Size = new System.Drawing.Size(130, 24);
            this.cbDefaults.TabIndex = 19;
            this.cbDefaults.Text = "Only non-defaults";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(21, 198);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 24);
            this.label6.TabIndex = 18;
            this.label6.Text = "Filter By Type:";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(21, 175);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 24);
            this.label4.TabIndex = 17;
            this.label4.Text = "Filter By fullname:";
            // 
            // txtFilterByType
            // 
            this.txtFilterByType.Location = new System.Drawing.Point(142, 198);
            this.txtFilterByType.Name = "txtFilterByType";
            this.txtFilterByType.Size = new System.Drawing.Size(197, 20);
            this.txtFilterByType.TabIndex = 16;
            this.txtFilterByType.Text = "*";
            // 
            // txtFilterByName
            // 
            this.txtFilterByName.Location = new System.Drawing.Point(142, 172);
            this.txtFilterByName.Name = "txtFilterByName";
            this.txtFilterByName.Size = new System.Drawing.Size(197, 20);
            this.txtFilterByName.TabIndex = 15;
            this.txtFilterByName.Text = "*";
            // 
            // cbRecursive
            // 
            this.cbRecursive.Location = new System.Drawing.Point(48, 103);
            this.cbRecursive.Name = "cbRecursive";
            this.cbRecursive.Size = new System.Drawing.Size(130, 24);
            this.cbRecursive.TabIndex = 13;
            this.cbRecursive.Text = "Recursive";
            // 
            // cbImportAll
            // 
            this.cbImportAll.Checked = true;
            this.cbImportAll.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbImportAll.Location = new System.Drawing.Point(48, 47);
            this.cbImportAll.Name = "cbImportAll";
            this.cbImportAll.Size = new System.Drawing.Size(191, 24);
            this.cbImportAll.TabIndex = 14;
            this.cbImportAll.Text = "Import all (for Excel file)";
            // 
            // lblProgress
            // 
            this.lblProgress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblProgress.BackColor = System.Drawing.SystemColors.Window;
            this.lblProgress.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProgress.Location = new System.Drawing.Point(56, 320);
            this.lblProgress.Name = "lblProgress";
            this.lblProgress.Size = new System.Drawing.Size(280, 24);
            this.lblProgress.TabIndex = 12;
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.lblProductVersion);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.lblAmplaVersion);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Location = new System.Drawing.Point(480, 636);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(352, 58);
            this.groupBox4.TabIndex = 14;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Version";
            // 
            // lblProductVersion
            // 
            this.lblProductVersion.Location = new System.Drawing.Point(235, 24);
            this.lblProductVersion.Name = "lblProductVersion";
            this.lblProductVersion.Size = new System.Drawing.Size(111, 16);
            this.lblProductVersion.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(176, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 18);
            this.label3.TabIndex = 3;
            this.label3.Text = "Product:";
            // 
            // lblAmplaVersion
            // 
            this.lblAmplaVersion.Location = new System.Drawing.Point(76, 24);
            this.lblAmplaVersion.Name = "lblAmplaVersion";
            this.lblAmplaVersion.Size = new System.Drawing.Size(94, 16);
            this.lblAmplaVersion.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(21, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 18);
            this.label2.TabIndex = 0;
            this.label2.Text = "Ampla: ";
            // 
            // lbAmpla
            // 
            this.lbAmpla.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbAmpla.HorizontalScrollbar = true;
            this.lbAmpla.Location = new System.Drawing.Point(504, 40);
            this.lbAmpla.Name = "lbAmpla";
            this.lbAmpla.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lbAmpla.Size = new System.Drawing.Size(304, 238);
            this.lbAmpla.TabIndex = 15;
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.Location = new System.Drawing.Point(480, 16);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(352, 280);
            this.groupBox5.TabIndex = 16;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Selected Items to Export";
            // 
            // cmdAdd
            // 
            this.cmdAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAdd.Location = new System.Drawing.Point(409, 213);
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Size = new System.Drawing.Size(56, 32);
            this.cmdAdd.TabIndex = 17;
            this.cmdAdd.Text = ">";
            this.cmdAdd.Click += new System.EventHandler(this.cmdAdd_Click);
            // 
            // cmdRemove
            // 
            this.cmdRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdRemove.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdRemove.Location = new System.Drawing.Point(409, 253);
            this.cmdRemove.Name = "cmdRemove";
            this.cmdRemove.Size = new System.Drawing.Size(56, 32);
            this.cmdRemove.TabIndex = 18;
            this.cmdRemove.Text = "<";
            this.cmdRemove.Click += new System.EventHandler(this.cmdRemove_Click);
            // 
            // cmdRemoveAll
            // 
            this.cmdRemoveAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdRemoveAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdRemoveAll.Location = new System.Drawing.Point(409, 293);
            this.cmdRemoveAll.Name = "cmdRemoveAll";
            this.cmdRemoveAll.Size = new System.Drawing.Size(56, 32);
            this.cmdRemoveAll.TabIndex = 19;
            this.cmdRemoveAll.Text = "<<";
            this.cmdRemoveAll.Click += new System.EventHandler(this.cmdRemoveAll_Click);
            // 
            // cmdSaveProject
            // 
            this.cmdSaveProject.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmdSaveProject.Location = new System.Drawing.Point(64, 710);
            this.cmdSaveProject.Name = "cmdSaveProject";
            this.cmdSaveProject.Size = new System.Drawing.Size(144, 32);
            this.cmdSaveProject.TabIndex = 20;
            this.cmdSaveProject.Text = "&Save Ampla Project";
            this.cmdSaveProject.Click += new System.EventHandler(this.cmdSaveProject_Click);
            // 
            // cmdReload
            // 
            this.cmdReload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmdReload.Location = new System.Drawing.Point(224, 710);
            this.cmdReload.Name = "cmdReload";
            this.cmdReload.Size = new System.Drawing.Size(144, 32);
            this.cmdReload.TabIndex = 21;
            this.cmdReload.Text = "&Reload Ampla Project";
            this.cmdReload.Click += new System.EventHandler(this.cmdReload_Click);
            // 
            // tvClass
            // 
            this.tvClass.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tvClass.FullRowSelect = true;
            this.tvClass.LabelEdit = true;
            this.tvClass.Location = new System.Drawing.Point(48, 450);
            this.tvClass.Name = "tvClass";
            this.tvClass.Size = new System.Drawing.Size(320, 190);
            this.tvClass.TabIndex = 22;
            this.tvClass.BeforeExpand += new System.Windows.Forms.TreeViewCancelEventHandler(this.tvClass_BeforeExpand);
            this.tvClass.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvClass_AfterSelect);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.lblProjectName);
            this.groupBox6.Controls.Add(this.label5);
            this.groupBox6.Location = new System.Drawing.Point(24, 420);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(368, 274);
            this.groupBox6.TabIndex = 23;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Class Explorer";
            // 
            // txtStatus
            // 
            this.txtStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStatus.BackColor = System.Drawing.SystemColors.Control;
            this.txtStatus.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtStatus.Location = new System.Drawing.Point(-2, 748);
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.Size = new System.Drawing.Size(862, 13);
            this.txtStatus.TabIndex = 24;
            // 
            // Form1
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(856, 766);
            this.Controls.Add(this.txtStatus);
            this.Controls.Add(this.tvClass);
            this.Controls.Add(this.cmdReload);
            this.Controls.Add(this.cmdSaveProject);
            this.Controls.Add(this.cmdRemoveAll);
            this.Controls.Add(this.cmdRemove);
            this.Controls.Add(this.cmdAdd);
            this.Controls.Add(this.lbAmpla);
            this.Controls.Add(this.lblProgress);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.cmdOK);
            this.Controls.Add(this.tvStudio);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox6);
            this.MinimumSize = new System.Drawing.Size(864, 650);
            this.Name = "Form1";
            this.Text = "PS Ampla Editor";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.Form1_Closing);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		#region Form thread entry point-----------------------------------------------------------

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
            Application.Run(new AppForm());
		}

		#endregion //entry point

		#region form methods and events-----------------------------------------------------------

		/// <summary>
		/// Form load event
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Form1_Load(object sender, EventArgs e)
		{

            AppDomain.CurrentDomain.SetPrincipalPolicy(PrincipalPolicy.WindowsPrincipal);
            using (StudioBootstrapper bootstrapper = new StudioBootstrapper())
            {
                bootstrapper.ConfiguringContainer(new ContainerBuilder());
                lifetimeScope = bootstrapper.GetLifetimeScope();
            }
            this.clientProjectFactory = lifetimeScope.Resolve<Func<Uri, Project.ProgressUpdate, Owned<ClientProject>>>();

            PermissionsToRun();
            RedirectAssemblies();
			NotificationTimers();
			mainLog = new Log();
			this.NodeClick += new System.Windows.Forms.TreeViewEventHandler(nodeClick);
			LoadAmplaPoject();
			this.cmdOK.Enabled = false;
			this.lblAmplaVersion.Text = GetAmplaVersion();
			this.lblProductVersion.Text = Application.ProductVersion;
		}
		
		/// <summary>
		/// Load Ampla Project
		/// </summary>
		private void LoadAmplaPoject()
		{
			this.cmdReload.Enabled = false;
			this.tvStudio.Nodes.Clear();
			this.UpdateStatusBar("Getting Ampla project........");
			project = null;
			GetAmplaProject gap = new GetAmplaProject(GetProject);
			IAsyncResult ar = gap.BeginInvoke(new AsyncCallback(MyCallback),null);
		}
		/// <summary>
		/// Callback method raised when Ampla project loaded
		/// </summary>
		/// <param name="ar"></param>
		private void MyCallback(IAsyncResult ar)
		{
			AsyncResult async = (AsyncResult)ar;
			GetAmplaProject gap = (GetAmplaProject)async.AsyncDelegate;
			try
			{
				bool result = gap.EndInvoke(ar);
				this.Invoke(new SetButtonEnable(SetEnable),new object[] {this.cmdReload, true});
				this.Invoke(new ResetNotificationText(ResetNotification));
				if (result)
				{
					if (this.txtFile.Text != string.Empty) this.Invoke(new SetButtonEnable(SetEnable),new object[] {this.cmdOK, true});
					this.Invoke(new SetLabelTextValue(SetLabelText),new object[] {this.lblProjectName,aproject.ProjectName});
					this.Invoke(new UpdateStatus(this.UpdateStatusBar), new object[] {"Ampla project loaded........"});
					projectloaded = true;
					this.Invoke(new FillTreeView(FillTree));
                    this.Invoke(new FillClassTreeView(FillClassTree));
				}
				else
				{
					this.Invoke(new UpdateStatus(this.UpdateStatusBar), new object[] { "Ampla project failed to load........"});
					MessageBox.Show(this,"Could not retrieve Ampla project or project not selected");
					//System.Environment.Exit(-1);

				}
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex.Message);
				mainLog.RaiseException(ex);
				mainLog.CloseFile();
			}
		}

		/// <summary>
		/// Log running task to get Ampla project
		/// </summary>
		/// <returns>Boolean success/failure</returns>
		private bool GetProject()
		{
			try
			{
				//this.Invoke(new Loading(ShowLoading),  new object[] { true });
                aproject = new AmplaProject(this, this.clientProjectFactory);
				Console.WriteLine("Getting server project.......................");
				project = aproject.GetProject();
				if (project == null) throw new ArgumentNullException();
				Console.WriteLine("Project loaded");
				return true;
			}
			catch (Exception ex)
			{
				Debug.Write(ex.Message);
				mainLog.RaiseException(ex);
				mainLog.CloseFile();
				return false;
			}
		}

		/// <summary>
		/// Set Enable property
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="Value"></param>
		private void SetEnable(Button cmd, bool Value)
		{
			cmd.Enabled = Value;
		}

		/// <summary>
		/// Set Label text
		/// </summary>
		/// <param name="lbl"></param>
		/// <param name="text"></param>
		private void SetLabelText(Label lbl, string text)
		{
			lbl.Text = text;
		}
		/// <summary>
		/// Form closing event
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Form1_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (process != null) process.CloseExcel();
			if (project != null)
			{
				mainLog.CloseFile();
				project.Dispose();
			}
		}

		/// <summary>
		/// Import all properties from Excel
		/// </summary>
		public bool ImportAll
		{
			get
			{
				return this.cbImportAll.Checked;
			}
		}
        /// <summary>
        /// Only non default properties
        /// </summary>
        public bool Defaults
        {
            get
            {
                return this.cbDefaults.Checked;
            }
        }
        /// <summary>
        /// Filter by Name properties
        /// </summary>
        public string NameFilter
        {
            get
            {
                return this.txtFilterByName.Text;
            }
        }
        /// <summary>
        /// FilterByPropertyName
        /// </summary>
        public string PNameFilter
        {
            get
            {
                return this.txtFilterByPName.Text;
            }
        }
        /// <summary>
        /// Filter By Type properties
        /// </summary>
        public string TypeFilter
        {
            get
            {
                return this.txtFilterByType.Text;
            }
        }
        /// <summary>
        /// Full path of the file to export to or import from 
        /// </summary>
        public string Filename
        {
            get
            {
                return this.txtFile.Text;
            }
        }
        public bool IncludeItems
        {
            get
            {
                return this.cbItems.Checked;
            }
        }
		#endregion //Form methods and events

		#region Project Tree view event and metods------------------------------------------------

		/// <summary>
		/// Populates tree view with Ampla project Items
		/// </summary>
		private void FillTree()
		{
			this.tvStudio.Nodes.Clear();
			ItemSorter.Sort(project.RootItems);
			
			foreach (Item child in ItemSorter.Sort(project.RootItems))
			{
				if (child.GetType().IsClass)
				{
					ParentNode pNode = new ParentNode(child.Name);
					pNode.Tag = child as object;
					pNode.Name = child.Name;
					pNode.Fullname = child.FullName;
                    pNode.ObjectType = child.GetType();
					this.Invoke(new AddNode(AddTreeNode), new object[] { this.tvStudio.Nodes, pNode });
					parentNode(pNode);
				}
			}
		}

		
		/// <summary>
		/// Add tree nodes
		/// </summary>
		/// <param name="nodes"></param>
		/// <param name="pNode"></param>
		private void AddTreeNode(TreeNodeCollection nodes,ParentNode pNode)
		{
			nodes.Add(pNode);
		}

		/// <summary>
		/// Set parent node
		/// </summary>
		/// <param name="node"></param>
		private void parentNode(TreeNode node)
		{
			ParentNode pn = (ParentNode)node;
			foreach (Item child in ItemSorter.Sort(project.AllItems[pn.Fullname].Items))
			{
				if (child.GetType().IsClass)
				{
					ParentNode pNode = new ParentNode(child.Name);
					pNode.Tag = child as object;
					pNode.Name = child.Name;
					pNode.Fullname = child.FullName;
                    pNode.ObjectType = child.GetType();
					this.Invoke(new AddNode(AddTreeNode), new object[] { node.Nodes, pNode });
				}
			}
		}

		/// <summary>
		/// Add subnodes
		/// </summary>
		/// <param name="node"></param>
		private void AddSubNodes(ParentNode node)
		{
			for (int i = 0; i < node.Nodes.Count; i++)
			{
				parentNode(node.Nodes[i]);
			}
			node.ChildrenAdded = true;
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tvStudio_BeforeExpand(object sender, System.Windows.Forms.TreeViewCancelEventArgs e)
		{
			ParentNode nodeExpanding = (ParentNode)e.Node;
			try
			{
				if (!nodeExpanding.ChildrenAdded)AddSubNodes(nodeExpanding);
			}
			catch (Exception ex)
			{
				MessageBox.Show(this,ex.Message);
			}
		}
		/// <summary>
		/// Tree view event: After Select
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tvStudio_AfterSelect(object sender, System.Windows.Forms.TreeViewEventArgs e)
		{
			Text = "Item selected - " + e.Node.FullPath.Replace("\\",".");
			fullname = string.Empty;
			NodeClick(this, e);
		}
		/// <summary>
		/// Tree view event: Node click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void nodeClick(object sender, System.Windows.Forms.TreeViewEventArgs e)
		{
			TreeNode node = (TreeNode)e.Node;
			if (node != null)
			{
				amplanode = node;
				fullname = node.FullPath.Replace("\\",".");
			}

		}

		/// <summary>
		/// Tree view click event
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tvStudio_Click(object sender, System.EventArgs e)
		{
			Text = "PS Ampla Editor";
			fullname = string.Empty;
			amplanode = null;
			tvStudio.SelectedNode = null;
		}

		#endregion

        #region Class Tree view event and metods--------------------------------------------------

        /// <summary>
        /// Populates tree view with Ampla project Items
        /// </summary>
        private void FillClassTree()
        {
            this.tvClass.Nodes.Clear();
            ClassDefinitionCollection classes = project.ClassDefinitions;


            ItemSorter.Sort(project.AllItems);

            foreach (ClassDefinition child in classes)
            {
                string fullname = child.FullName;
                string[] className = fullname.Split('.');
                //for (int i = 0; i < names.Length; i++)
                //{

                if (className.Length == 1)
                {
                    ParentNode pNode = new ParentNode(child.Name);
                    pNode.Tag = child as object;
                    pNode.Name = child.Name;
                    pNode.Fullname = child.FullName;
                    pNode.ObjectType = child.GetType();
                    pNode.Id = child.Id;
                    pNode.SubClassDefinitions = child.GetSubClassDefinitions();
                    this.Invoke(new AddNode(AddClassTreeNode), new object[] { this.tvClass.Nodes, pNode });
                    parentClassNode(pNode);
                }
            }
        }


        /// <summary>
        /// Add tree nodes
        /// </summary>
        /// <param name="nodes"></param>
        /// <param name="pNode"></param>
        private void AddClassTreeNode(TreeNodeCollection nodes, ParentNode pNode)
        {
            nodes.Add(pNode);
        }

        /// <summary>
        /// Set parent node
        /// </summary>
        /// <param name="node"></param>
        private void parentClassNode(TreeNode node)
        {
            ParentNode pn = (ParentNode)node;
            foreach (ClassDefinition child in pn.SubClassDefinitions)
            {
                if (true)
                {
                    ParentNode pNode = new ParentNode(child.Name);
                    pNode.Tag = child as object;
                    pNode.Name = child.Name;
                    pNode.Fullname = child.FullName;
                    pNode.ObjectType = child.GetType();
                    pNode.Id = child.Id;
                    pNode.SubClassDefinitions = child.GetSubClassDefinitions();
                    this.Invoke(new AddNode(AddClassTreeNode), new object[] { node.Nodes, pNode });
                }
            }
        }

        /// <summary>
        /// Add subnodes
        /// </summary>
        /// <param name="node"></param>
        private void AddClassSubNodes(ParentNode node)
        {
            for (int i = 0; i < node.Nodes.Count; i++)
            {
                parentClassNode(node.Nodes[i]);
            }
            node.ChildrenAdded = true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tvClass_BeforeExpand(object sender, System.Windows.Forms.TreeViewCancelEventArgs e)
        {
            ParentNode nodeExpanding = (ParentNode)e.Node;
            try
            {
                if (!nodeExpanding.ChildrenAdded) AddClassSubNodes(nodeExpanding);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message);
            }
        }
        /// <summary>
        /// Tree view event: After Select
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tvClass_AfterSelect(object sender, System.Windows.Forms.TreeViewEventArgs e)
        {
            Text = "Item selected - " + e.Node.FullPath.Replace("\\", ".");
            fullname = string.Empty;
            ClassNodeClick(this, e);
        }
        /// <summary>
        /// Tree view event: Node click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClassNodeClick(object sender, System.Windows.Forms.TreeViewEventArgs e)
        {
            TreeNode node = (TreeNode)e.Node;
            if (node != null)
            {
                amplanode = node;
                fullname = node.FullPath.Replace("\\", ".");
            }

        }

        /// <summary>
        /// Tree view click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tvClass_Click(object sender, System.EventArgs e)
        {
            Text = "PS Ampla Editor";
            fullname = string.Empty;
            amplanode = null;
            tvClass.SelectedNode = null;
        }

        #endregion

		#region Animation-------------------------------------------------------------------------
        /// <summary>
        /// Shows message box
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="text"></param>
        private void ShowMessageBox(IWin32Window owner, string text)
        {
            MessageBox.Show(owner, text);
        }
		/// <summary>
		/// Create timers for progress display
		/// </summary>
		private void NotificationTimers()
		{
			onTimer = new System.Timers.Timer(ontime);
			offTimer = new System.Timers.Timer(offtime);
			onTimer.Elapsed += new ElapsedEventHandler(OnTimer);
			offTimer.Elapsed += new ElapsedEventHandler(OffTimer);
			onTimer.Enabled = false;
			offTimer.Enabled = false;
		}

		/// <summary>
		/// Animation display
		/// </summary>
		/// <param name="visible"></param>
		public void ShowLoading(bool visible)
		{
			lblProgress.Visible = visible;
			if (!visible) lblProgress.Text = string.Empty;
		}

		/// <summary>
		/// Animation On display
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTimer(object sender, System.Timers.ElapsedEventArgs e)
		{
			if (this.InvokeRequired)
			{
				this.Invoke(new ConcatenateText(ConcatText));
			}
			else
			{
				ConcatText();
			}
		}

		/// <summary>
		/// Animation Off display
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OffTimer(object sender, System.Timers.ElapsedEventArgs e)
		{
			System.Timers.Timer offtimer = (System.Timers.Timer) sender;
			offtimer.Enabled = false;
			if (this.InvokeRequired)
			{
				this.Invoke(new SetOnTimer(SetThisTimer), new object[] { true });
			}
			else
			{
				SetThisTimer(true);
			}
			//onTimer.Enabled = true;
		}


		private void SetThisTimer(bool enable)
		{
			this.onTimer.Enabled = enable;
		}
		/// <summary>
		/// Append character to label
		/// </summary>
		private void ConcatText()
		{
			char[] ctext = text.ToCharArray();
			if (index == 0) lblProgress.ResetText();
			if (index < ctext.Length)
			{
				lblProgress.Text = string.Concat(lblProgress.Text, ctext[index]);
				index++;
			}
			if (index == ctext.Length)
			{
				this.onTimer.Enabled = false;
				this.offTimer.Enabled = true;
				index = 0;
			}
			lblProgress.Refresh();
		}
		
		/// <summary>
		/// Set animation with specified string
		/// </summary>
		/// <param name="Text"></param>
		public void SetNotification(string Text)
		{
			index = 0;
			if (Text != string.Empty) text = Text;
			onTimer.Enabled = true;

			//this.Invoke(new Loading(ShowLoading), new object[] { true });
			ShowLoading(true);
		}

		/// <summary>
		/// Reset animation
		/// </summary>
		public void ResetNotification()
		{
			//this.Invoke(new Loading(ShowLoading),  new object[] {false });
			ShowLoading(false);
			onTimer.Enabled = false;
			offTimer.Enabled = false;
		}
		#endregion //Animation

		#region Methods and events (buttons)------------------------------------------------------

		/// <summary>
		/// File browser button click event
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void cmdBrowser_Click(object sender, EventArgs e)
		{

			if (this.rbExport.Checked)
			{
				SaveFileDialog fdlg = new SaveFileDialog();
				fdlg.Title = "Select Excel File Dialog";
                fdlg.Filter = "Excel (*.xls)|*.xls|Xml files (*.xls)|*.xls";
				fdlg.FilterIndex = 1;
				fdlg.RestoreDirectory = true;
				if (fdlg.ShowDialog() == DialogResult.OK)
				{
					this.txtFile.Text = fdlg.FileName;
				} 
			}
			else
			{
				OpenFileDialog fdlg = new OpenFileDialog();
                fdlg.Title = "Select Excel File Dialog";
                fdlg.Filter = "Excel (*.xls)|*.xls|Xml files (*.xls)|*.xls";
                //fdlg.FilterIndex = 1;
				fdlg.RestoreDirectory = true;
				if (fdlg.ShowDialog() == DialogResult.OK)
				{
					this.txtFile.Text = fdlg.FileName;
				} 
			}
            
		}
		
		/// <summary>
		/// OK button click event
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void cmdOK_Click(object sender, EventArgs e)
		{
			IAsyncResult ear;
			IAsyncResult iar;
			ExportProperties export;
			ImportProperties import;
            List<AmplaItem> amplaitems = new List<AmplaItem>();

            options = new Options(this);
            process = new Process(project, mainLog,options);
			process.Filename = this.txtFile.Text;
			if (this.rbExport.Checked)
			{
               
				if (this.lbAmpla.Items.Count == 0) return;
                foreach (ParentNode node in this.lbAmpla.Items)
                {
                    AmplaItem item = new AmplaItem();
                    item.Id = node.Id;
                    item.Fullname = node.Fullname;
                    item.ObjectType = node.ObjectType;
                    amplaitems.Add(item);
                }
				cmdOK.Enabled = false;
				tvStudio.CollapseAll();
				SetNotification("Exporting Properties............");
				process.isExport = true;
				process.isRecursive = this.cbRecursive.Checked;
				process.Fullname = fullname;
                process.ObjectToProcess = amplaitems;
				export = new ExportProperties(process.ExportProperties);
				ear = export.BeginInvoke(this.cbRecursive.Checked,amplaitems,new AsyncCallback(ExportCallback), null);
			}
			else
			{
				cmdOK.Enabled = false;
				tvStudio.CollapseAll();
				SetNotification("Importing Properties............");
				process.isImport = true;
				import = new ImportProperties(process.ImportProperties);
				iar = import.BeginInvoke(new AsyncCallback(ImportCallback), null);
			}
		}

		/// <summary>
		/// Long running task to export properties
		/// </summary>
		/// <param name="ear"></param>
		private void ExportCallback(IAsyncResult ear)
		{
			AsyncResult async = (AsyncResult)ear;
			ExportProperties exp = (ExportProperties)async.AsyncDelegate;
			try
			{
				bool result = exp.EndInvoke(ear);
				this.Invoke(new SetButtonEnable(SetEnable),new object[] {this.cmdOK, true});
				this.Invoke(new ResetNotificationText(ResetNotification));
				if (result)
				{
					this.Invoke(new UpdateStatus(this.UpdateStatusBar), new object[] {"Export process completed"});
					this.Invoke(new ResetCheckBoxesValue(ResetCheckBoxes));
                    this.Invoke(new ShowMessageBox(this.ShowMessageBox), new object[] { this, "Export process completed" });
					//MessageBox.Show(this,"Export process completed");
				}
				else
				{
					this.Invoke(new UpdateStatus(this.UpdateStatusBar), new object[] {"Export process failed"});
                    this.Invoke(new ShowMessageBox(this.ShowMessageBox), new object[] { this, "Export process failed, check log file" });
					//MessageBox.Show(this,"Export process failed, check log file");
				}
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex.Message);
				mainLog.RaiseException(ex);
				mainLog.CloseFile();
			}
		}

		/// <summary>
		/// Long running task to import properties
		/// </summary>
		/// <param name="iar"></param>
		private void ImportCallback(IAsyncResult iar)
		{
			AsyncResult async = (AsyncResult)iar;
			ImportProperties imp = (ImportProperties)async.AsyncDelegate;
			try
			{
				bool result = imp.EndInvoke(iar);
                this.Invoke(new SetButtonEnable(SetEnable), new object[] { this.cmdOK, true });
				this.Invoke(new ResetNotificationText(ResetNotification));
				if (result)
				{
					this.Invoke(new UpdateStatus(this.UpdateStatusBar), new object[] {"Import process completed"});
					this.Invoke(new ResetCheckBoxesValue(ResetCheckBoxes));
					this.Invoke(new FillTreeView(FillTree));
                    this.Invoke(new ShowMessageBox(this.ShowMessageBox), new object[] { this, "Import process completed" });
				}
				else
				{
					this.Invoke(new UpdateStatus(this.UpdateStatusBar), new object[] {"Import process failed"});
                    this.Invoke(new ShowMessageBox(this.ShowMessageBox), new object[] { this, "Import process failed, check log file" });
				}
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex.Message);
			}
		}

		/// <summary>
		/// Delete selected Ampla item
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void cmdSaveProject_Click(object sender, System.EventArgs e)
		{
			SetNotification("Saving Project............");
			this.txtStatus.Text = " Saving process in progress";
			this.cmdSaveProject.Enabled = false;
			SaveAmplaProject saveproject = new SaveAmplaProject(SaveProject);
			IAsyncResult savear = saveproject.BeginInvoke(new AsyncCallback(SaveProjectCallback), null);
		}

		private bool SaveProject()
		{
			try
			{
				if (new AmplaProject().IsCompiled(project,mainLog))
				{
					project.SaveChanges();
					return true;
				}
				return false;
			}
			catch(Exception e)
			{
				mainLog.RaiseException(e);
				return false;
			}
		}

		private void SaveProjectCallback(IAsyncResult savear)
		{
			AsyncResult async = (AsyncResult)savear;
			SaveAmplaProject save = (SaveAmplaProject)async.AsyncDelegate;
			try
			{
				bool result = save.EndInvoke(savear);
				this.Invoke(new ResetNotificationText(ResetNotification));
				this.Invoke(new SetButtonEnable(SetEnable),new object[] {this.cmdSaveProject, true});
				if (result)
				{
					this.Invoke(new ResetCheckBoxesValue(ResetCheckBoxes));
					this.Invoke(new UpdateStatus(this.UpdateStatusBar), new object[] {"Saving process succeded"});
                    this.Invoke(new ShowMessageBox(this.ShowMessageBox), new object[] { this, "Save Ampla project completed" });
					//MessageBox.Show(this,"Save Ampla project completed");
				}
				else
				{
					this.Invoke(new UpdateStatus(this.UpdateStatusBar), new object[] {"Saving process failed"});
					//MessageBox.Show(this,"Saving process failed, for further info check log file");
                    this.Invoke(new ShowMessageBox(this.ShowMessageBox), new object[] { this, "Saving process failed, for further info check log file" });
				}
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex.Message);
				mainLog.RaiseException(ex);
				mainLog.CloseFile();
			}
		}
		/// <summary>
		/// Cancel button click event
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void cmdCancel_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		/// <summary>
		/// Reset Recursive and Import All checkboxes
		/// </summary>
		private void ResetCheckBoxes()
		{
			this.cbImportAll.Checked = false;
			this.cbRecursive.Checked = false;
		}

		/// <summary>
		/// Reload Ampla Project
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void cmdReload_Click(object sender, System.EventArgs e)
		{
			if (this.cmdReload.Enabled)
			{
				LoadAmplaPoject();
			}
		}
		#endregion buttons

		#region Other form objects----------------------------------------------------------------

		/// <summary>
		/// Excel filename textbox text changed event
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void txtFile_TextChanged(object sender, EventArgs e)
		{
			if (txtFile.Text != string.Empty && projectloaded) this.cmdOK.Enabled = true;
		}

		/// <summary>
		/// Update status bar with specified string
		/// </summary>
		/// <param name="name"></param>
		public void UpdateStatusBar(string name)
		{
			this.txtStatus.Text = " " + name;
		}

		/// <summary>
		/// Toggle import radio button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void rbExport_CheckedChanged(object sender, System.EventArgs e)
		{
			if (this.rbExport.Checked) this.rbImport.Checked = false;
		}

		/// <summary>
		/// Toggle export radio button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void rbImport_CheckedChanged(object sender, System.EventArgs e)
		{
			if (this.rbImport.Checked) this.rbExport.Checked = false;
		}

		#endregion //Other form objects

		#region Other methods and properties------------------------------------------------------
		/// <summary>
		/// Validate open excel file
		/// </summary>
		/// <returns>Success or failure</returns>
		private string GetAmplaDirectory()
		{
            AppSettingsReader reader = new AppSettingsReader();
            string amplaDirectory = reader.GetValue("AmplaDirectory", typeof(System.String)).ToString();
            if (Directory.Exists(@amplaDirectory))
			{
                return @amplaDirectory;
			}
			else
			{
                return string.Empty;
			}
		}

		/// <summary>
		/// Gets Ampla version from Ampla dll
		/// </summary>
		/// <returns></returns>
		private string GetAmplaVersion()
		{
			string AmplaDirectory = GetAmplaDirectory();	
			DirectoryInfo dir = new DirectoryInfo(AmplaDirectory);
			FileInfo[] dllfiles = dir.GetFiles("*.dll");
			foreach (FileInfo f in dllfiles)
			{
				if (f.Name.StartsWith("Citect.Ampla"))
				{
					return FileVersionInfo.GetVersionInfo(f.FullName).FileVersion.ToString();
				}
			}
			return string.Empty;
		}

		/// <summary>
		/// Gets the buid number of Ampla for this product
		/// </summary>
		/// <returns></returns>
		private string GetBuildAmpla()
		{
			Assembly a = Assembly.GetExecutingAssembly();
			foreach (AssemblyName an in a.GetReferencedAssemblies())
			{
				if (an.Name.StartsWith("Citect.Ampla.General"))
				{
					return an.Version.ToString();
				}
			}
			return string.Empty;
		}

		#endregion //Other methods and properties

		#region Permissions to run----------------------------------------------------------------

		/// <summary>
		/// Get the permissions to load application:
		/// If executable in the right folder
		/// If the dll version correct
		/// If administrator user
		/// If code security added
		/// </summary>
		private void PermissionsToRun()
		{
			//if (!AppBase()||!IsVersion() || !IsAdministrator()) System.Environment.Exit(-1); 
			if (!AppBase()|| !IsAdministrator()) System.Environment.Exit(-1); 
			//if (!CodeSecurityAdded()) System.Environment.Exit(-1);
		}

		/// <summary>
		/// Check if executable is located in Ampla folder
		/// </summary>
		/// <returns></returns>
		private bool AppBase()
		{
			#if (!DEBUG)
				AppDomain ad = AppDomain.CurrentDomain;
				string AmplaDirectory = GetAmplaDirectory();
				string BaseDirectory = ad.BaseDirectory;

				if (BaseDirectory.EndsWith(@"\")) BaseDirectory = BaseDirectory.Substring(0,BaseDirectory.Length - 1);
				if (AmplaDirectory.EndsWith(@"\")) AmplaDirectory = AmplaDirectory.Substring(0,AmplaDirectory.Length - 1);;
				if (AmplaDirectory == BaseDirectory)
				{
					return true;
				}
				else
				{
					MessageBox.Show(this,"Copy PSAmplaEditor.exe to Ampla directory!",
						BaseDirectory + " : " + AmplaDirectory
						);
					return false;

				}
			#else
				return true;
			#endif
		}

		/// <summary>
		/// Check if Citect/Ampla version of exe are the same than the installed Ampla
		/// </summary>
		/// <returns></returns>
		private bool IsVersion()
		{
			string AmplaDirectory = GetAmplaDirectory();	
			bool version = false;

			if (AmplaDirectory == string.Empty)
			{
				MessageBox.Show(this,"Ampla directory not found!","Ampla Directory");
				return false;
			}
			else
			{
				DirectoryInfo dir = new DirectoryInfo(AmplaDirectory);
				FileInfo[] dllfiles = dir.GetFiles("*.dll");
				

				Assembly a = Assembly.GetExecutingAssembly();
				foreach (AssemblyName an in a.GetReferencedAssemblies())
				{
					if (an.Name.StartsWith("Citect"))
					{
						foreach (FileInfo f in dllfiles)
						{
							if (f.Name == an.Name + ".dll")
							{
								if (FileVersionInfo.GetVersionInfo(f.FullName).FileVersion.ToString()!= an.Version.ToString())
								{
									MessageBox.Show(this,f.Name + ": version mistmatch!","Wrong Ampla version");
									return false;
								}
								else
								{
									version = true;
									break;
								}
							}
						}
						if (!version)
						{
							MessageBox.Show(this,an.Name + ": file not found!","dll file not found");
							return false;
						}
						version = false;
					}
				}
			}
			return true;
		}

		/// <summary>
		/// Checks if Windows user is administrator
		/// </summary>
		/// <returns></returns>
		private bool IsAdministrator()
		{
			WindowsIdentity identity = WindowsIdentity.GetCurrent();
			WindowsPrincipal principal = new WindowsPrincipal(identity);
			bool ret = principal.IsInRole(WindowsBuiltInRole.Administrator);
			if (!ret) MessageBox.Show(this,"You are not an Administrator!","Windows Login User");
			return ret;
		}
		
		/// <summary>
		/// Check if runtime security for the assembly path has been added, if not add the code group
		/// with full trust level
		/// </summary>
		/// <returns></returns>
        private bool CodeSecurityAdded()
        {
            if (GetSecurity())
            {
                if (!IsCodeGroup())
                {
                    RegisterCodeGroupSecurity();
                    return true;
                }
            }
            else
            {
                MessageBox.Show(this, "Unable to get the security policy user level", "Unable to get user security");
                return false;
            }
            return true;
        }

		/// <summary>
		/// Checks if the security already exists for the code group
		/// </summary>
		/// <returns></returns>
		private bool IsCodeGroup()
		{
			foreach (CodeGroup codeGroup in UserCodeGroupRoot.Children)
			{
				if (codeGroup.Name == Application.ProductName)
				{
					return true;
				}
			}
			return false;
		}

		/// <summary>
		/// Gets the framework security policy
		/// </summary>
		/// <returns></returns>
        private bool GetSecurity()
        {
            try
            {
                IEnumerator secLevels = SecurityManager.PolicyHierarchy();

                while (secLevels.MoveNext())
                {
                    PolicyLevel level = secLevels.Current as PolicyLevel;
                    if (level != null && level.Label == "User")
                    {
                        policyUserLevel = level;
                        UserCodeGroupRoot = level.RootCodeGroup;
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                mainLog.RaiseException(ex);
                mainLog.CloseFile();
            }
            return false;
        }

		/// <summary>
		/// Register the code group in the runtime section of the framework security for code items
		/// </summary>
        private void RegisterCodeGroupSecurity()
        {
            string targetfile = "file://" + Environment.GetEnvironmentVariable("windir").Replace("\\", "/") + "/assembly/*";
            myCodeGroup = new UnionCodeGroup(new UrlMembershipCondition(targetfile), new PolicyStatement(new NamedPermissionSet("FullTrust")));
            myCodeGroup.Name = Application.ProductName;
            UserCodeGroupRoot.AddChild(myCodeGroup);
            SecurityManager.SavePolicyLevel(policyUserLevel);

        }

		/// <summary>
		/// Removes the security for the code group - not used
		/// </summary>
        private void DeleteCodeGroup()
        {
            if (GetSecurity())
            {
                UserCodeGroupRoot.RemoveChild(myCodeGroup);
                SecurityManager.SavePolicyLevel(policyUserLevel);
            }
        }

		#endregion //Permissions

        #region RedirectAssemblies ---------------------------------------------------------------

        private void RedirectAssemblies()
        {
            Redirection redirection = new Redirection(this.mainLog);
            if (!redirection.AddRedirection())
            {
                MessageBox.Show(this, "Unable to redirect assemblies from build to current", "Unable to redirect assemblie");
            }
        }


        #endregion //RedirectAssemblies

        #region ListBox properties and methods----------------------------------------------------

        /// <summary>
		/// Add item to listbox
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void cmdAdd_Click(object sender, System.EventArgs e)
		{
			if (amplanode != null)
			{
				TreeNode node = (ParentNode) amplanode;
				if (!this.lbAmpla.Items.Contains(node))
				{
					this.lbAmpla.Items.Add(node);
				
				}
			}

		}

		/// <summary>
		/// Remove item(s) from listbox
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void cmdRemove_Click(object sender, System.EventArgs e)
		{
			while (this.lbAmpla.SelectedIndices.Count > 0)
			{
				this.lbAmpla.Items.RemoveAt(this.lbAmpla.SelectedIndices[0]);
			}
		}

		/// <summary>
		/// Remove all selected items
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void cmdRemoveAll_Click(object sender, System.EventArgs e)
		{
			this.lbAmpla.Items.Clear();
		}

		#endregion //ListBox

		#region Copy/Paste/Delete/Rename and context menu-----------------------------------------

		/// <summary>
		/// Create context menu if right click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tvStudio_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if(e.Button == MouseButtons.Right) 
 			{ 
				tvStudio.SelectedNode = tvStudio.GetNodeAt (e.X ,e.Y );
				if (tvStudio.SelectedNode == null) return;
				if (cm ==null)
				{
					cm = new ContextMenu();
					cm.MenuItems.Add(new MenuItem("&Copy", new EventHandler(this.mcopy_Click)));
					cm.MenuItems.Add(new MenuItem("&Paste", new EventHandler(this.mpaste_Click)));
					cm.MenuItems.Add(new MenuItem("&Delete", new EventHandler(this.mdelete_Click)));
					cm.MenuItems.Add(new MenuItem("&Rename", new EventHandler(this.mrename_Click)));
					cm.MenuItems[1].Enabled = false;	//paste menu item
				}
				cm.Show(this.tvStudio,new Point(e.X,e.Y));
			}
		}

		/// <summary>
		/// Click copy in menu
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void mcopy_Click(object sender, System.EventArgs e)
		{
			ParentNode pn = (ParentNode) tvStudio.SelectedNode;
			Item item = (Item) project.AllItems[pn.Fullname];

			string itemAsXml = item.CopyToXml();
			DataObject dataObject = new DataObject(clipboardFormat.Name, itemAsXml);					
			System.Windows.Forms.Clipboard.SetDataObject(dataObject, true);
			cm.MenuItems[1].Enabled = true;	//paste menu item
			//MessageBox.Show(this,"Selected item copied");

		}

		/// <summary>
		/// Click paste in menu
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void mpaste_Click(object sender, System.EventArgs e)
		{
			SetNotification("Pasting Item............");
			IDataObject dataObject = Citect.Ampla.Framework.Gui.Clipboard.GetDataObject();
			this.cmdReload.Enabled = false;
			PasteItem pasteitem = new PasteItem(PasteAmplaItem);
			IAsyncResult pastear = pasteitem.BeginInvoke(dataObject,new AsyncCallback(PasteItemCallback),null);
		
		}

		/// <summary>
		/// Not used
		/// </summary>
		/// <returns></returns>
		private bool PasteAmplaItem(IDataObject ido)
		{
			bool nodefound = false;
			try
			{
				ParentNode pn = (ParentNode) tvStudio.SelectedNode;
				Item item = (Item) project.AllItems[pn.Fullname];
				string itemAsXml = (string)ido.GetData(clipboardFormat.Name);

				if (itemAsXml != null && itemAsXml.Length > 0)
				{
					if (item == null)
					{
						project.PasteFromXml(itemAsXml);
					}
					else
					{
						item.PasteFromXml(itemAsXml);
					}
				}
				foreach(Item child in project.AllItems[pn.Fullname].Items)
				{
					nodefound = false;
					foreach(ParentNode node in pn.Nodes)
					{
						if (child.FullName == node.Fullname)
						{
							nodefound = true;
							break;
						}
					}
					if (!nodefound)
					{
						nodefound = false;
						ParentNode newNode = new ParentNode(child.Name);
						newNode.Tag = child as object;
						newNode.Name = child.Name;
						newNode.Fullname = child.FullName;
						newNode.ObjectType = child.GetType();
						this.Invoke(new AddNode(AddTreeNode), new object[] { pn.Nodes, newNode });
					}
				}
				return true;
			}
			catch (Exception ex)
			{
				this.mainLog.RaiseException(ex);
				mainLog.CloseFile();
				return false;
			}
		}

		/// <summary>
		/// Not used
		/// </summary>
		/// <param name="pastear"></param>
		private void PasteItemCallback(IAsyncResult pastear)
		{
			AsyncResult async = (AsyncResult)pastear;
			PasteItem paste = (PasteItem)async.AsyncDelegate;
			try
			{
				bool result = paste.EndInvoke(pastear);

				this.Invoke(new SetButtonEnable(SetEnable),new object[] {this.cmdReload, true});
				this.Invoke(new SetMenuItemEnable(SetMenuItem),new object[] {this.cm.MenuItems[1], true});
				this.Invoke(new ResetNotificationText(ResetNotification));
				if (result)
				{
					this.Invoke(new UpdateStatus(this.UpdateStatusBar), new object[] {"Ampla item pasted"});
					MessageBox.Show(this,"Ampla item successfully pasted");
					this.Invoke(new ResetCheckBoxesValue(ResetCheckBoxes));
				}
				else
				{
					this.Invoke(new UpdateStatus(this.UpdateStatusBar), new object[] {"Saving process failed"});
					MessageBox.Show(this,"Paste item failed, check log file");
				}
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex.Message);
				mainLog.CloseFile();
			}
		}

		/// <summary>
		/// SetMenuItem Enable
		/// </summary>
		/// <param name="mi"></param>
		/// <param name="Value"></param>
		private void SetMenuItem(MenuItem mi, bool Value)
		{
			mi.Enabled = Value;
		}

		/// <summary>
		/// Click delete in menu
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void mdelete_Click(object sender, System.EventArgs e)
		{
			ParentNode node = (ParentNode)this.tvStudio.SelectedNode;
			if (node != null)
			{
				DialogResult result = MessageBox.Show(this,string.Concat("Do you want to delete Ampla item ", node.Fullname),
					"Delete Ampla item",MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (result == DialogResult.Yes)
				{
					try
					{
						if (project.AllItems[node.Fullname].CanDelete)
						{
							this.lbAmpla.Items.Clear();
							Item item = project.AllItems[node.Fullname];
							ItemNode inode = new ItemNode(item);
							item.Delete();
							node.Remove();
							tvStudio.Refresh();
						}
					}
					catch(Exception ex)
					{
						Debug.WriteLine(ex.Message);
						mainLog.RaiseException(ex);
						mainLog.CloseFile();
					}
				}
			}
		}

		/// <summary>
		/// Click rename in menu
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void mrename_Click(object sender, System.EventArgs e)
		{
			ParentNode pn = (ParentNode) tvStudio.SelectedNode;
			Item item = (Item) project.AllItems[pn.Fullname];
			
			if(!pn.IsEditing && item.CanRename)
			{
				pn.BeginEdit();
			}
			
		}

		/// <summary>
		/// Renaming item
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tvStudio_AfterLabelEdit(object sender, System.Windows.Forms.NodeLabelEditEventArgs e)
		{

			
			if (e.Label != null)
			{
				if(e.Label.Length > 0)
				{
					if (e.Label.IndexOfAny(new char[]{'@', '.', ',', '!'}) == -1)
					{
						// Stop editing without canceling the label change.
						e.Node.EndEdit(false);
					}
					else
					{
						/* Cancel the label edit action, inform the user, and 
						   place the node in edit mode again. */
						e.CancelEdit = true;
						MessageBox.Show("Invalid tree node label.\n" + 
							"The invalid characters are: '@','.', ',', '!'", 
							"Node Label Edit");
						e.Node.BeginEdit();
					}
				}
				else
				{
					/* Cancel the label edit action, inform the user, and 
					   place the node in edit mode again. */
					e.CancelEdit = true;
					MessageBox.Show("Invalid tree node label.\nThe label cannot be blank", 
						"Node Label Edit");
					e.Node.BeginEdit();
				}
				
				e.Node.Text = e.Label;
				ParentNode thisnode = (ParentNode)e.Node;
				Item item = (Item) project.AllItems[thisnode.Fullname];
				PropertyInfo pinfo = item.GetType().GetProperty("Name");
				pinfo.SetValue(project.AllItems[thisnode.Fullname],e.Label,null);
				thisnode.Fullname = item.FullName;
			}
		}
 		
		#endregion //Context menu
 
    }
}
