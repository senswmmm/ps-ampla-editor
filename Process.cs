//------------------------------------------------------------------------------------------------
//  Copyright (c) 2006 Citect Pty Limited.
//  All rights reserved. 
//	
//	History:
//			23/05/2006		Julio Montufar		Original
//------------------------------------------------------------------------------------------------
using System;
using System.Data;
using System.Data.OleDb;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Drawing;
using System.Diagnostics;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;
using System.Threading;

#region Citect/Ampla references----------------------------------------------------------------------------

using Citect.Common;
using Citect.Common.Globalization;
using Citect.Ampla.Framework;
using Citect.Ampla.Framework.ClassSystem;
using Citect.Ampla.General.Server;
using Citect.Ampla.General.Server.UserInterface;
using Citect.Ampla.General.Server.TimeDefinition;
using Citect.Ampla.General.Server.Views;
using Helper = Citect.Ampla.General.Server.Helper;
using CommonHelper = Citect.Ampla.General.Common.Helper;
using Citect.Ampla.General.Common;
using Citect.Security.Identities;
using Citect.Ampla.StandardItems;
using OleDbConnector = Citect.Ampla.Connectors.OleDbConnector;
//using Citect.Ampla.Scripting;
using Citect.Ampla.Framework.Scripting;
using Citect.Ampla.Framework.ItemLocationSystem;
using Citect.Ampla.Runtime.Remoting.Scripting;
using Citect.Ampla.Notification.Server;
using Citect.Ampla.Data;

#endregion //Ampla references

namespace PSAmplaEditor
{
    class Process
    {
        #region fields-------------------------------------------------------------------------------------

        private string projectname = string.Empty;
        private string datareporsitory = string.Empty;
        private Excel.Application App;
        private Excel.Workbook Workbook;
        private Excel.Worksheet Sheet;
        private Excel.Sheets Sheets;
        private int colTypeName;
        private int colFullName;
        private int colPropertyName;
        private int colPropertyValue;
        private ClientProject project;
        private int count = 1;
        private int totalCount = 1;
        private int row = 2;
        private string fileName = string.Empty;
        private string fullName = string.Empty;
        private string typeName = string.Empty;
        private List<AmplaItem> objectToProcess;
        private string propertyName = string.Empty;
        private string propertyValue = string.Empty;
        private string CollectionName = string.Empty;
        private string exportField = string.Empty;
        private string importField = string.Empty;
        private string reCursive = string.Empty;
        private string nameFilter = string.Empty;
        private string typeFilter = string.Empty;
        private string pnameFilter = string.Empty;
        private bool includeItems;
        private bool export;
        private bool import;
        private bool recursive;
        private Log mainLog;
        private bool defaults;
        private bool importAll;
        private Options options;
        private DataSet outputDataSet;
        private DataSet inputDataSet;
        private string imported = "Imported";
        private string singleImport = "Import";
        #endregion //fields

        #region constants----------------------------------------------------------------------------------

        private const string textFormatSpecifier = "@";
        private const string exportSheet = "Ampla";
        private const string importSheet = "Ampla";
        private const int maxColumns = 20;
        private const int intrinsicColumnCount = 6;
        private const string hTypename = "TypeName";
        private const string hFullname = "FullName";
        private const string hPropertyname = "PropertyName";
        private const string hPropertyvalue = "PropertyValue";
        private const string EXCEL_CHAR_TOREPLACE = "-";
        private const string EXCEL_CHAR_REPLACEBY = "#";

        #endregion //constants

        #region constructors-------------------------------------------------------------------------------

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="Project"></param>
        /// <param name="log"></param>
        public Process(ClientProject Project, Log log,Options options)
        {
            this.options = options;
            this.project = Project;
            this.mainLog = log;
            this.nameFilter = options.NameFilter;
            this.typeFilter = options.TypeFilter;
            this.pnameFilter = options.PNameFilter;
            this.includeItems = options.IncludeItems;
            this.defaults = options.Defaults;
            this.importAll = options.ImportAll;
            this.fileName = options.FileName;
        }

        #endregion

        #region Export methods-----------------------------------------------------------------------------

        /// <summary>
        /// Export Ampla properties to Excel - main call
        /// </summary>
        /// <param name="fullname"></param>
        /// <param name="isrecursive"></param>
        /// <returns></returns>
        public bool ExportProperties1(bool isrecursive, string[] fullname)
        {
            bool ret = false;
            count = 0;
            totalCount = 0;
            row = 2;

            try
            {
                mainLog.WriteLine(string.Concat("project ", project.DatabaseName, " open"));
                options.UpdateStatus("Ampla project loaded");
                GetExcelWorkbook(Filename, exportSheet);
                mainLog.WriteLine("Getting and exporting items.........................\n");
                populateColumnHeader();
                CreateOutput();
                formatPropertyValueColumn(exportSheet);
                DateTime start = DateTime.Now;
                //for (int i = 0; i < fullname.Length; i++) GetItems((Item)project.AllItems[fullname[i]], isrecursive);
                DateTime stop = DateTime.Now;
                TimeSpan duration = stop - start;
                ExportOutput();
                mainLog.WriteLine(string.Concat(count, " records exported\r\nProcessing Time: " + duration.ToString()));
                ret = true;
            }
            catch (Exception e)
            {
                mainLog.RaiseException(e);
                CloseExcel();
            }
            finally
            {
                CloseExcel();
                mainLog.CloseFile();
            }
            return ret;
        }
        public bool ExportProperties(bool isrecursive, List<AmplaItem> amplaItems)
        {
            bool ret = false;

            count = 0;
            totalCount = 0;
            row = 2;

            try
            {
                mainLog.WriteLine(string.Concat("project ", project.DatabaseName, " open"));
                options.UpdateStatus("Ampla project loaded");
                mainLog.WriteLine("Getting and exporting items.........................\n");
                CreateOutput();
                DateTime start = DateTime.Now;
                foreach (AmplaItem item in amplaItems) GetItems(item, isrecursive);
                DateTime stop = DateTime.Now;
                TimeSpan duration = stop - start;
                if (fileName.Contains(".xml"))
                {
                    outputDataSet.Tables[0].WriteXml(fileName);
                }
                else if (fileName.Contains(".xls"))
                {
                    GetExcelWorkbook(Filename, exportSheet);
                    populateColumnHeader();
                    formatPropertyValueColumn(exportSheet);
                    ExportOutput();
                    FormatTopRow();
                    FormatAutoFit();
                }
                
                mainLog.WriteLine(string.Concat(count, " records exported\r\nProcessing Time: " + duration.ToString()));
                ret = true;
            }
            catch (Exception e)
            {
                mainLog.RaiseException(e);
                if (fileName.Contains(".xls")) CloseExcel();
            }
            finally
            {
                if (fileName.Contains(".xls")) CloseExcel();
                mainLog.CloseFile();
            }
            return ret;
        }

        /// <summary>
        /// Export properties to Excel
        /// </summary>
        /// <param name="fullname"></param>
        /// <param name="recursive"></param>
        private void GetItems(AmplaItem amplaItem, bool recursive)
        {

            string fullname = amplaItem.Fullname;
            string typename = amplaItem.ObjectType.ToString();
            if (typename.IndexOf("ClassDefinition") > 0)
            {
                GetClassItems(amplaItem, recursive);
            }
            else
            {
                GetProjectItems(fullname, recursive);
            }
        }

        /// <summary>
        /// Export properties to Excel
        /// </summary>
        /// <param name="fullname"></param>
        /// <param name="recursive"></param>
        private void GetClassItems(AmplaItem amplaItem, bool recursive)
        {
            Type AmplaType;
            object o;
            object propertyValue;

            o = project.ClassDefinitions.FindById(amplaItem.Id);
            AmplaType = amplaItem.ObjectType;
            string fullname = amplaItem.Fullname;

            if (AmplaType.IsClass)
            {
                if (includeItems && TypeFilter(typeFilter, AmplaType.FullName) && NameFilter(nameFilter, fullname)) AddOutputRow(AmplaType.FullName, fullname, string.Empty, string.Empty);
                foreach (PropertyInfo propertyInfo in AmplaType.GetProperties())
                {
                   if ((propertyInfo.CanWrite || IsContained("IncludeList", propertyInfo.Name)) && !propertyInfo.PropertyType.IsInterface &&
                        !propertyInfo.PropertyType.IsArray && !IsContained("ExceptionList", propertyInfo.Name) && NameFilter(nameFilter, fullname) &&
                        TypeFilter(typeFilter, AmplaType.FullName) && NameFilter(pnameFilter, propertyInfo.Name)
                        )
                    {
                        string propertyName = propertyInfo.Name;
                        try
                        {
                            propertyValue = propertyInfo.GetValue(o, null) as object;
                            if (propertyValue == null) { propertyValue = string.Empty; }

                            if (IsContained("CollectionItems", propertyValue.ToString()))
                            {
                                GetClassCollection(propertyValue, propertyName, fullname);
                            }
                            else
                            {
                                if (options.Defaults)
                                {
                                    ItemPropertyDescriptorCollection collection = project.AllItems[fullname].PropertyDescriptors;
                                    ItemPropertyDescriptor descriptor = GetDescriptor(collection, propertyName);
                                    string currentValue = GetValue(propertyValue, propertyInfo.PropertyType, o as Item);

                                    if (descriptor != null)
                                    {
                                        string defaultValue = (descriptor.DefaultValue == null) ? string.Empty : descriptor.DefaultValue.ToString();
                                        if (currentValue != defaultValue)
                                        {
                                            AddOutputRow(AmplaType.FullName, fullname, propertyName, currentValue);
                                        }
                                    }
                                }
                                else
                                {
                                    AddOutputRow(AmplaType.FullName, fullname, propertyName, GetValue(propertyValue, propertyInfo.PropertyType, o as Item));
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            mainLog.WriteLine(string.Concat("Record ", count.ToString()));
                            if (AmplaType != null) mainLog.WriteLine(string.Concat("Type: ", AmplaType.Name));
                            mainLog.WriteLine(string.Concat("Fullname: ", fullname));
                            if (propertyName != string.Empty) mainLog.WriteLine(string.Concat("Property: ", propertyName));
                            mainLog.RaiseException(ex);
                            mainLog.CloseFile();
                        }
                    }
 
                }
                if (recursive)
                {
                    List<ClassDefinition> children = project.ClassDefinitions.FindById(amplaItem.Id).GetSubClassDefinitions();
                    foreach (ClassDefinition item in children)
                    {
                        AmplaItem childItem = new AmplaItem();
                        childItem.Fullname = item.FullName;
                        childItem.ObjectType = item.GetType();
                        childItem.Id = item.Id;

                        if (item.GetType().IsClass) GetClassItems(childItem, recursive);
                    }
                }
            }
        }

        /// <summary>
        /// Export properties to Excel
        /// </summary>
        /// <param name="fullname"></param>
        /// <param name="recursive"></param>
        private void GetProjectItems(string fullname, bool recursive)
        {
            Type AmplaType;
            object o;
            object propertyValue;

            o = project.AllItems[fullname];
            AmplaType = project.AllItems[fullname].GetType();
            if (AmplaType.IsClass)
            {
                if (includeItems && TypeFilter(typeFilter, AmplaType.FullName) && NameFilter(nameFilter, fullname)) AddOutputRow(AmplaType.FullName, fullname, string.Empty, string.Empty);
                foreach (PropertyInfo propertyInfo in AmplaType.GetProperties())
                {
                    if ((propertyInfo.CanWrite || IsContained("IncludeList", propertyInfo.Name)) && !propertyInfo.PropertyType.IsInterface &&
                        !propertyInfo.PropertyType.IsArray && !IsContained("ExceptionList", propertyInfo.Name) && NameFilter(nameFilter, fullname) &&
                        TypeFilter(typeFilter, AmplaType.FullName) && NameFilter(pnameFilter, propertyInfo.Name)
                        )
                    {
                        string propertyName = propertyInfo.Name;
                        try
                        {
                            propertyValue = propertyInfo.GetValue(o, null) as object;
                            if (propertyValue == null) { propertyValue = string.Empty; }
                            if (IsContained("NestedItems", propertyValue.ToString()))
                            {
                                GetNestedProperties(propertyValue, propertyName, fullname);
                            }
                            if (IsContained("CollectionItems", propertyValue.ToString()))
                            {
                                GetNestedCollection(propertyValue, propertyName, fullname);
                            }
                            //Changes made to export only non default values
                            //================================================
                            if (IsContained("IncludeList", propertyInfo.Name))
                            {
                                AddOutputRow(AmplaType.FullName, fullname, propertyName, GetValue(propertyValue, propertyInfo.PropertyType, o as Item));
                            }
                            else
                            {
                                if (options.Defaults)
                                {
                                    ItemPropertyDescriptorCollection collection = project.AllItems[fullname].PropertyDescriptors;
                                    ItemPropertyDescriptor descriptor = GetDescriptor(collection,propertyName);
                                    string currentValue = GetValue(propertyValue, propertyInfo.PropertyType, o as Item);

                                    if (descriptor != null)
                                    {
                                        string defaultValue = (descriptor.DefaultValue == null) ? string.Empty : descriptor.DefaultValue.ToString();
                                        if (currentValue != defaultValue)
                                        {
                                            AddOutputRow(AmplaType.FullName, fullname, propertyName, currentValue);
                                        }
                                    }
                                    else
                                    {
                                        AddOutputRow(AmplaType.FullName, fullname, propertyName, currentValue);
                                    }
                                }
                                else
                                {
                                    AddOutputRow(AmplaType.FullName, fullname, propertyName, GetValue(propertyValue, propertyInfo.PropertyType, o as Item));
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            mainLog.WriteLine(string.Concat("Record ", count.ToString()));
                            if (AmplaType != null) mainLog.WriteLine(string.Concat("Type: ", AmplaType.Name));
                            mainLog.WriteLine(string.Concat("Fullname: ", fullname));
                            if (propertyName != string.Empty) mainLog.WriteLine(string.Concat("Property: ", propertyName));
                            mainLog.RaiseException(ex);
                            mainLog.CloseFile();
                        }
                    }
                }
                if (AmplaType.Name == "User") GetExtendedUserProperties(o as Item);
                if (recursive)
                {
                    foreach (Item item in Helper.ItemSorter.Sort(project.AllItems[fullname].Items))
                    {
                        if (item.GetType().IsClass) GetProjectItems(item.FullName, recursive);
                    }
                }
            }
        }

        private ItemPropertyDescriptor GetDescriptor(ItemPropertyDescriptorCollection collection, string propertyname)
        {
            foreach (ItemPropertyDescriptor descriptor in collection)
            {
                if (descriptor.Name.ToLower() == propertyname.ToLower()) return descriptor;

            }
            return null;
        }

        /// <summary>
        /// Get properties for nested types
        /// </summary>
        /// <param name="propertyvalue"></param>
        /// <param name="fname"></param>
        /// 
        private void GetNestedProperties(object propertyvalue, string propertyname, string fname)
        {
            switch (propertyvalue.ToString())
            {
                case "(Relationship Matrix)":
                    //GetNestedCollection(propertyvalue, propertyname,fname);
                    GetNestedRelationshipMatrix(propertyvalue, propertyname, fname);
                    break;
            }
        }

        /// <summary>
        /// Get collectiom properties
        /// </summary>
        /// <param name="propertyvalue"></param>
        /// <param name="propertyname"></param>
        /// <param name="fname"></param>
        private void GetNestedCollection(object propertyvalue, string propertyname, string fname)
        {
            object propertyValue;

            CollectionBase al = (CollectionBase)propertyvalue;
            foreach (object o in al)
            {
                string name = o.GetType().GetProperty("Name").GetValue(o, null).ToString();
                AddOutputRow(o.GetType().FullName, string.Concat(propertyvalue.ToString(), "|", fname, "|", propertyname, "|", name), string.Empty, string.Empty);
                foreach (PropertyInfo pinfo in o.GetType().GetProperties())
                {

                    if (row == ushort.MaxValue - 1) return;
                    try
                    {
                        if (pinfo.CanWrite && !pinfo.PropertyType.IsInterface &&
                            !pinfo.PropertyType.IsArray && !IsContained("ExceptionList", pinfo.Name))
                        {
                            propertyValue = pinfo.GetValue(o, null);
                            AddOutputRow(pinfo.PropertyType.FullName, string.Concat(propertyvalue.ToString(), "|", fname, "|", propertyname, "|", name),
                                pinfo.Name, GetValue(propertyValue, pinfo.PropertyType, o as Item));
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
        }


        /// <summary>
        /// Get collectiom properties
        /// </summary>
        /// <param name="propertyvalue"></param>
        /// <param name="propertyname"></param>
        /// <param name="fname"></param>
        private void GetClassCollection(object propertyvalue, string propertyname, string fname)
        {
            object propertyValue;

            PropertyDefinitionCollection al = (PropertyDefinitionCollection)propertyvalue;
            foreach (PropertyDefinition o in al)
            {
                string name = o.GetType().GetProperty("Name").GetValue(o, null).ToString();
                AddOutputRow(o.GetType().FullName, string.Concat(propertyvalue.ToString(), "|", fname, "|", propertyname, "|", name), string.Empty, string.Empty);
                foreach (PropertyInfo pinfo in o.GetType().GetProperties())
                {

                    if (row == ushort.MaxValue - 1) return;
                    try
                    {
                        if (pinfo.CanWrite && !pinfo.PropertyType.IsInterface &&
                            !pinfo.PropertyType.IsArray && !IsContained("ExceptionList", pinfo.Name))
                        {
                            propertyValue = pinfo.GetValue(o, null);
                            AddOutputRow(pinfo.PropertyType.FullName, string.Concat(propertyvalue.ToString(), "|", fname, "|", propertyname, "|", name),
                                pinfo.Name, propertyValue.ToString());
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
        }

        /// <summary>
        /// Get relatioship matrix properties
        /// </summary>
        /// <param name="propertyvalue"></param>
        /// <param name="propertyname"></param>
        /// <param name="fname"></param>
        private void GetNestedRelationshipMatrix(object propertyvalue, string propertyname, string fname)
        {

            try
            {
                for (int i = 0; i < ((Matrix)propertyvalue).Entries.Length; i++)
                {
                    MatrixEntry me = (MatrixEntry)((Matrix)propertyvalue).Entries[i];
                    if ((object)me.CauseCodeId != null && me.CauseCodeId.ToString().ToLower().IndexOf("null") == -1)
                    {
                        AddOutputRow(me.CauseCodeId.GetType().FullName, string.Concat(propertyvalue.ToString(), "|", fname, "|", propertyname, "|", i),
                            "CauseCodeId", GetNameFromId(me.CauseCodeId, "CauseCode"));
                    }
                    if ((object)me.ClassificationId != null && me.ClassificationId.ToString().ToLower().IndexOf("null") == -1)
                    {
                        AddOutputRow(me.ClassificationId.GetType().FullName, string.Concat(propertyvalue.ToString(), "|", fname, "|", propertyname, "|", i),
                            "ClassificationId", GetNameFromId(me.ClassificationId, "Classification"));
                    }
                    if ((object)me.EffectId != null && me.EffectId.ToString().ToLower().IndexOf("null") == -1)
                    {
                        AddOutputRow(me.EffectId.GetType().FullName, string.Concat(propertyvalue.ToString(), "|", fname, "|", propertyname, "|", i),
                            "EffectId", GetNameFromId(me.EffectId, "Effect"));
                    }
                }
            }
            catch { }
        }
        /// <summary>
        /// Write out user extended properties
        /// </summary>
        /// <param name="item"></param>
        private void GetExtendedUserProperties(Item item)
        {
            UserExtenderProvider user = new UserExtenderProvider();
            //FavoriteDescriptorCollection favorites = user.GetFavorites(item);
            //string[] fav = new string[favorites.Count];
            //for (int i = 0; i < items.Count; i++) fav[i] = favorites[i];
            //string sFavorites = string.Join(",", fav);
            //if (NameFilter(nameFilter, item.FullName) && TypeFilter(typeFilter, item.TypeName)) AddOutputRow(item.TypeName, item.FullName, "Favorites", sFavorites);

            //if (NameFilter(nameFilter, item.FullName) && TypeFilter(typeFilter, item.TypeName)) AddOutputRow(item.TypeName, item.FullName, "Favorites", sFavorites);

            if (NameFilter(nameFilter, item.FullName) && TypeFilter(typeFilter, item.TypeName) && NameFilter(pnameFilter, "AutoRefreshData")) AddOutputRow(item.TypeName, item.FullName, "AutoRefreshData", user.GetAutoRefreshData(item).ToString());
            if (NameFilter(nameFilter, item.FullName) && TypeFilter(typeFilter, item.TypeName) && NameFilter(pnameFilter, "CanViewChart")) AddOutputRow(item.TypeName, item.FullName, "CanViewChart", user.GetCanViewChart(item).ToString());
            if (NameFilter(nameFilter, item.FullName) && TypeFilter(typeFilter, item.TypeName) && NameFilter(pnameFilter, "CanViewFilter")) AddOutputRow(item.TypeName, item.FullName, "CanViewFilter", user.GetCanViewFilter(item).ToString());
            if (NameFilter(nameFilter, item.FullName) && TypeFilter(typeFilter, item.TypeName) && NameFilter(pnameFilter, "CanViewFolderSelector")) AddOutputRow(item.TypeName, item.FullName, "CanViewFolderSelector", user.GetCanViewFolderSelector(item));
            if (NameFilter(nameFilter, item.FullName) && TypeFilter(typeFilter, item.TypeName) && NameFilter(pnameFilter, "CanViewGrid")) AddOutputRow(item.TypeName, item.FullName, "CanViewGrid", user.GetCanViewGrid(item).ToString());
            //Change implementation in 4.1
            //============================
            System.Collections.Generic.IEnumerable<ItemLink> items = user.GetHierarchies(item).GetItemLinks();
            string hierarchies = string.Empty;
            foreach (ItemLink iLink in items)
            {
                hierarchies = hierarchies + iLink.AbsolutePath + ",";
            }
            hierarchies.TrimEnd(new char[] { ',' });
            //string[] cs = new string[items.Count];
            //for (int i = 0; i < items.Count; i++) cs[i] = items[i];
            //string hierarchies = string.Join(",", cs);
            if (NameFilter(nameFilter, item.FullName) && TypeFilter(typeFilter, item.TypeName) && NameFilter(pnameFilter, "Hierarchies")) AddOutputRow(item.TypeName, item.FullName, "Hierarchies", hierarchies);

            if (NameFilter(nameFilter, item.FullName) && TypeFilter(typeFilter, item.TypeName) && NameFilter(pnameFilter, "HistoryCache")) AddOutputRow(item.TypeName, item.FullName, "HistoryCache", user.GetHistoryCache(item).ToString());
            if (NameFilter(nameFilter, item.FullName) && TypeFilter(typeFilter, item.TypeName) && NameFilter(pnameFilter, "ShowFavoritesFolder")) AddOutputRow(item.TypeName, item.FullName, "ShowFavoritesFolder", user.GetShowFavoritesFolder(item).ToString());
            if (NameFilter(nameFilter, item.FullName) && TypeFilter(typeFilter, item.TypeName) && NameFilter(pnameFilter, "ShowHomeFolder")) AddOutputRow(item.TypeName, item.FullName, "ShowHomeFolder", user.GetShowHomeFolder(item).ToString());

            if (NameFilter(nameFilter, item.FullName) && TypeFilter(typeFilter, item.TypeName) && NameFilter(pnameFilter, "CanSaveFavorites")) AddOutputRow(item.TypeName, item.FullName, "CanSaveFavorites", user.GetCanSaveFavorites(item).ToString());
            if (NameFilter(nameFilter, item.FullName) && TypeFilter(typeFilter, item.TypeName) && NameFilter(pnameFilter, "CanSaveHome")) AddOutputRow(item.TypeName, item.FullName, "CanSaveHome", user.GetCanSaveHome(item).ToString());

        }


        /// <summary>
        /// Get the name from key
        /// </summary>
        /// <param name="id"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private string GetNameFromId(int id, string type)
        {
            Hashtable h = null;

            if (type == "CauseCode") h = CauseCode.GetNameList(project);
            if (type == "Classification") h = Classification.GetNameList(project);
            if (type == "Effect") h = Effect.GetNameList(project);

            IDictionaryEnumerator en = h.GetEnumerator();
            en.Reset();
            while (en.MoveNext())
            {
                if ((int)en.Key == id) return en.Value.ToString();
            }
            return null;
        }

        private string GetNameFromId(string id, string type)
        {
            Hashtable h = null;

            if (type == "CauseCode") h = CauseCode.GetNameList(project);
            if (type == "Classification") h = Classification.GetNameList(project);
            if (type == "Effect") h = Effect.GetNameList(project);

            IDictionaryEnumerator en = h.GetEnumerator();
            en.Reset();
            while (en.MoveNext())
            {
                if ((string)en.Key == id) return en.Value.ToString();
            }
            return null;
        }

        /// <summary>
        /// Get the key from name
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        /// 
        private string GetStringIdFromName(string name, string type)
        {
            Hashtable h = null;
            int count = 0;
            string ret = "-1"; ;

            if (type == "CauseCode") h = CauseCode.GetNameList(project);
            if (type == "Classification") h = Classification.GetNameList(project);
            if (type == "Effect") h = Effect.GetNameList(project);
            IDictionaryEnumerator en = h.GetEnumerator();
            while (en.MoveNext())
            {
                if (en.Value.ToString() == name)
                {
                    ret = (string)en.Key;
                    count++;
                }
            }
            if (count > 1)
            {
                ret = "-1";
            }
            return ret;
        }

        private int GetIdFromName(string name, string type)
        {
            Hashtable h = null;
            int count = 0;
            int ret = -1;

            if (type == "CauseCode") h = CauseCode.GetNameList(project);
            if (type == "Classification") h = Classification.GetNameList(project);
            if (type == "Effect") h = Effect.GetNameList(project);
            IDictionaryEnumerator en = h.GetEnumerator();
            while (en.MoveNext())
            {
                if (en.Value.ToString() == name)
                {
                    ret = (int)en.Key;
                    count++;
                }
            }
            if (count > 1)
            {
                ret = -1;
            }
            return ret;
        }


        /// <summary>
        /// Find specific value in value string
        /// </summary>
        /// <param name="key"></param>
        /// <param name="svalue"></param>
        /// <returns></returns>
        private bool IsContained(string key, string svalue)
        {
            System.Configuration.AppSettingsReader reader = new System.Configuration.AppSettingsReader();
            String[] propArr = reader.GetValue(key, typeof(System.String)).ToString().Split(new Char[] { ',' });
            for (int i = 0; i < propArr.Length; i++)
            {
                if (svalue == propArr[i].ToString()) return true;
            }
            return false;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private string GetKeyValue(string key)
        {
            System.Configuration.AppSettingsReader reader = new System.Configuration.AppSettingsReader();
            return reader.GetValue(key, typeof(System.String)).ToString();
        }
        #endregion //Export

        #region Filter Logic ------------------------------------------------------------------------------

        /// <summary>
        /// This function takes a filter string and a fullName to be
        /// checked, and returns true if the filtered string passes
        /// the filter criteria.
        /// The filter string can be defined as a list of one or
        /// more filters:
        /// 
        ///      <filter1>|<filter2>|<filter3>|...
        /// 
        /// Each filter test that a string starts with, ends with, contains
        /// or equals a specified string:
        ///      
        ///      Starts With:    sample*
        ///      Ends With:      *sample
        ///      Contains:       *sample*
        ///      Equals:         sample
        /// 
        /// An empty filter string, or an asterix will return true 
        /// for any fullName.
        /// </summary>
        /// <param name="nameFilter"></param>
        /// <param name="fullName"></param>
        /// <returns></returns>
        private bool NameFilter(string nameFilter, string fullName)
        {
            bool match = false;
            bool wildcardStart, wildcardStop;
            string wildcard = "*";

            nameFilter = nameFilter.ToUpper();
            fullName = fullName.ToUpper();

            string[] names = nameFilter.Split('|');
            for (int i = 0; i < names.Length; i++)
            {
                //if (names[i].Trim() == "*" ||
                //    fullName.IndexOf(names[i].Trim()) >= 0 ||
                //    names[i].Trim() == "")
                //{
                //    match = true;
                //    break;
                //}

                names[i] = names[i].Trim();
                wildcardStart = names[i].StartsWith("*");
                wildcardStop = names[i].EndsWith("*");
                names[i] = names[i].Trim(wildcard.ToCharArray());

                if (names[i] == "")
                {
                    match = true;
                    break;
                }
                else if (wildcardStart && wildcardStop)
                {
                    if (fullName.Contains(names[i]))
                    {
                        match = true;
                        break;
                    }
                }
                else if (wildcardStart)
                {
                    if (fullName.EndsWith(names[i]))
                    {
                        match = true;
                        break;
                    }
                }
                else if (wildcardStop)
                {
                    if (fullName.StartsWith(names[i]))
                    {
                        match = true;
                        break;
                    }
                }
                else
                {
                    if (fullName.Equals(names[i]))
                    {
                        match = true;
                        break;
                    }
                }
            }

            return match;
        }

        /// <summary>
        /// This function takes a filter string and a typeName to be
        /// checked, and returns true if the filtered string passes
        /// the filter criteria.
        /// The filter string can be defined as a list of one or
        /// more filters:
        /// 
        ///      <filter1>|<filter2>|<filter3>|...
        /// 
        /// Each filter test that a string starts with, ends with, contains
        /// or equals a specified string:
        ///      
        ///      Starts With:    sample*
        ///      Ends With:      *sample
        ///      Contains:       *sample*
        ///      Equals:         sample
        /// 
        /// An empty filter string, or an asterix will return true 
        /// for any typeName.
        /// </summary>
        /// <param name="typeFilter"></param>
        /// <param name="typeName"></param>
        /// <returns></returns>
        private bool TypeFilter(string typeFilter, string typeName)
        {
            bool match = false;
            bool wildcardStart, wildcardStop;
            string wildcard = "*";

            typeFilter = typeFilter.ToUpper();
            typeName = typeName.ToUpper();

            string[] typeFilterParts = typeFilter.Split('|');
            for (int i = 0; i < typeFilterParts.Length; i++)
            {
                //if (typeFilterParts[i].Trim() == "*" ||
                //    typeName.IndexOf(typeFilterParts[i].Trim()) >= 0 ||
                //    typeFilterParts[i].Trim() == "")
                //{
                //    match = true;
                //    break;
                //}

                typeFilterParts[i] = typeFilterParts[i].Trim();
                wildcardStart = typeFilterParts[i].StartsWith("*");
                wildcardStop = typeFilterParts[i].EndsWith("*");
                typeFilterParts[i] = typeFilterParts[i].Trim(wildcard.ToCharArray());

                if (typeFilterParts[i] == "")
                {
                    match = true;
                    break;
                }
                else if (wildcardStart && wildcardStop)
                {
                    if (typeName.Contains(typeFilterParts[i]))
                    {
                        match = true;
                        break;
                    }
                }
                else if (wildcardStart)
                {
                    if (typeName.EndsWith(typeFilterParts[i]))
                    {
                        match = true;
                        break;
                    }
                }
                else if (wildcardStop)
                {
                    if (typeName.StartsWith(typeFilterParts[i]))
                    {
                        match = true;
                        break;
                    }
                }
                else
                {
                    if (typeName.Equals(typeFilterParts[i]))
                    {
                        match = true;
                        break;
                    }
                }
            }

            return match;
        }

        #endregion // Filter Logic---------------------

        #region Import methods-----------------------------------------------------------------------------

        /// <summary>
        /// Import properties - main call
        /// </summary>
        /// <returns></returns>
        public bool ImportProperties()
        {
            try
            {
                mainLog.WriteLine(string.Concat("project ", project.DatabaseName, " open"));
                options.UpdateStatus("Ampla project loaded");
                CreateInput();
                if (Filename.Contains(".xls"))
                {
                    if (!OpenExcel(Filename)) return false;
                    if (!ValidateFile()) return false;
                    ImportInput();
                    ImportFileProperties();
                    UpdateImportedExcel();
                }
                else if (Filename.Contains(".xml"))
                {
                    inputDataSet.ReadXml(Filename, XmlReadMode.Auto);
                    ImportFileProperties();
                    CreateXmlImportResultsFile();
                }
                else
                {
                    options.UpdateStatus("Wrong file type: file format should be xls or xml");
                    return false;
                }
                if (Filename.Contains(".xls")) CloseExcel();
                mainLog.CloseFile();
                return true;
            }
            catch (Exception e)
            {
                mainLog.WriteLine(string.Concat(count, " records written out of ", totalCount));
                mainLog.RaiseException(e);
                mainLog.CloseFile();
                if (Filename.Contains(".xls")) CloseExcel();
                return false;
            }

        }

        /// <summary>
        /// Import file properties - main call
        /// </summary>
        /// <returns></returns>
        public bool ImportFileProperties()
        {
            bool ret = false;
            //typeName = string.Empty;
            //fullName = string.Empty; ;
            //propertyName = string.Empty; ;
            //propertyValue = string.Empty; ;

            row = 0;        //changed from 2 to 0
            count = 0;
            totalCount = 0;

            try
            {
                mainLog.WriteLine(string.Concat("project ", project.DatabaseName, " open"));
                options.UpdateStatus("Ampla project loaded");
                //CreateInput();

                if (IsContained("RebuildMatrix", "True"))
                {
                    //Deletes all the Items from the equipment Types folder in the system configuration
                    Item etItem = project.AllItems["System Configuration.Equipment Types"] as Item;
                    if (etItem != null)
                    {
                        foreach (Item item in Helper.ItemSorter.Sort(etItem.Items))
                        {
                            item.Delete();
                        }
                    }

                    //Deletes all the Items from the Cause Codes folder either in the system configuration or lookup lists
                    if (IsContained("CauseCodes", "System Configuration.Lookup Lists"))
                    {
                        foreach (Item item in Helper.ItemSorter.Sort(project.AllItems["System Configuration.Lookup Lists.Cause Codes"].Items))
                        {
                            item.Delete();
                        }
                    }
                    else
                    {
                        Item item = project.AllItems[GetKeyValue("CauseCodes")] as Item;
                        item.Delete();
                    }

                    //Deletes all the Items from the Classifcations folder either in the system configuration or lookup lists
                    if (IsContained("Classifications", "System Configuration.Lookup Lists"))
                    {
                        Item cItem = project.AllItems["System Configuration.Lookup Lists.Classifications"] as Item;
                        foreach (Item item in Helper.ItemSorter.Sort(cItem.Items))
                        {
                            item.Delete();
                        }
                    }
                    else
                    {
                        Item item = project.AllItems[GetKeyValue("Classifications")] as Item;
                        item.Delete();
                    }

                    //Deletes all the Items from the Effects folder in the lookup list
                    foreach (Item item in Helper.ItemSorter.Sort(project.AllItems["System Configuration.Lookup Lists.Effects"].Items))
                    {
                        item.Delete();
                    }
                }
                mainLog.WriteLine("Getting records...........................\n");
                bool importAll = options.ImportAll;
                bool saveByRow = IsContained("SaveByRow", "True");

                for (int i = 0; i < inputDataSet.Tables["Data"].Rows.Count; i++)
                {
                    DataRow dr = inputDataSet.Tables["Data"].Rows[i];
                    if (importAll || IsImportRow(dr))
                    {
                        UpdateObject(dr);
                        if (saveByRow) project.SaveChanges();
                    }
                    row++;
                }
                options.UpdateStatus("Compiling project for errors....");
                bool validateBeforeSave = (bool.TryParse(GetKeyValue("ValidateBeforeSave"), out validateBeforeSave)) ? validateBeforeSave : true;
                if (!validateBeforeSave || new AmplaProject().IsCompiled(project, mainLog))
                {
                    options.UpdateStatus("Saving project....");
                    project.SaveChanges();
                    mainLog.WriteLine("Ampla project saved");
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                mainLog.WriteLine(string.Concat(count, " records written out of ", totalCount));
                mainLog.RaiseException(e);
                if (Filename.Contains(".xls")) CloseExcel();
                return false;
            }
        }

        /// <summary>
        /// Close and kill Excel instance
        /// </summary>
        public void CloseExcel()
        {
            try
            {
                if (App != null) App.DisplayAlerts = false;
                if (Workbook != null) Workbook.Close(true, this.Filename, null);
                if (App != null) App.Quit();
                if (Sheet != null) Marshal.ReleaseComObject(Sheet);
                if (Sheets != null) Marshal.ReleaseComObject(Sheets);
                if (App.Workbooks != null) Marshal.ReleaseComObject(App.Workbooks);
                if (Workbook != null) Marshal.ReleaseComObject(Workbook);
                if (App != null) Marshal.ReleaseComObject(App);
                if (Sheet != null) Sheet = null;
                if (Sheets != null) Sheets = null;
                if (Workbook != null) Workbook = null;
                if (App != null) App = null;
                GC.GetTotalMemory(false);
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.GetTotalMemory(true);
            }
            catch { }
        }
        //private void GetNextExcelRow()
        //{
        //    typeName = getCellString(row, colTypeName);
        //    fullName = getCellString(row, colFullName);
        //    propertyName = getCellString(row, colPropertyName);
        //    propertyValue = getCellString(row, colPropertyValue);
        //}

        /// <summary>
        /// Update Ampla Configuration
        /// </summary>
        /// <param name="typename"></param>
        /// <param name="fullname"></param>
        /// <param name="propertyname"></param>
        /// <param name="propertyvalue"></param>
        private void UpdateObject(DataRow dr)
        {
            string typename = dr[0].ToString();
            string fullname = dr[1].ToString();
            string propertyname = dr[2].ToString();
            string propertyvalue = dr[3].ToString();

            totalCount++;
            Type AmplaType = null;
            PropertyInfo propertyInfo = null;
            options.UpdateStatus(string.Concat("Fullname: ", fullname, " Property: ", propertyname));

            try
            {
                string[] split = fullname.ToString().Split(new Char[] { '|' });
                if (split.Length > 1)
                {
                    if (IsContained("NestedItems", split[0].ToString()))
                    {
                        UpdateAddNested(typename, fullname, propertyname, propertyvalue);
                    }
                    if (IsContained("CollectionItems", split[0].ToString()))
                    {
                        UpdateAddCollection(typename, fullname, propertyname, propertyvalue);
                    }
                }
                else
                {
                    Item aic = project.AllItems[fullname];
                    if (aic != null)
                    {
                        AmplaType = aic.GetType();
                        if (propertyname != null && propertyname != string.Empty)
                        {
                            propertyInfo = AmplaType.GetProperty(propertyname);
                            if (propertyInfo != null)
                            {
                                UpdateProperty(propertyInfo, propertyvalue, project.AllItems[fullname], propertyname, fullname);
                            }
                            else
                            {
                                if (AmplaType.Name == "User")
                                {
                                    SetExtendedUserProperties(aic, fullname, propertyname, propertyvalue);
                                }
                            }
                        }
                        else
                        {
                            count++;
                        }
                    }
                    else
                    {
                        if (propertyname == "")
                        {
                            //assume that new item is required, if a typo f#$%....
                            AddItem(typename, fullname);
                            RecordImported(row);
                        }
                        count++;
                    }
                }
            }
            catch (Exception ex)
            {
                mainLog.RaiseException(ex);
                mainLog.CloseFile();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="typename"></param>
        /// <param name="fullname"></param>
        /// <param name="propertyname"></param>
        /// <param name="propertyvalue"></param>
        private void UpdateAddNested(string typename, string fullname, string propertyname, string propertyvalue)
        {
            string[] split = fullname.ToString().Split(new Char[] { '|' });
            if (string.IsNullOrEmpty(propertyname))
            {
                UpdateAddNestedItem(split[0].ToString(), typename, fullname);
            }
            else
            {
                UpdateNestedProperties(split[0].ToString(), fullname, propertyname, propertyvalue);
            }
        }

        /// <summary>
        /// Add or Update collection
        /// </summary>
        /// <param name="typename"></param>
        /// <param name="fullname"></param>
        /// <param name="propertyname"></param>
        /// <param name="propertyvalue"></param>
        private void UpdateAddCollection(string typename, string fullname, string propertyname, string propertyvalue)
        {
            if (string.IsNullOrEmpty(propertyname))
            {
                UpdateAddCollectionItem(typename, fullname);
            }
            else
            {
                UpdateCollection(propertyname, fullname, propertyvalue);
            }
        }

        /// <summary>
        /// Update properties of nested items
        /// </summary>
        /// <param name="typename"></param>
        /// <param name="fullname"></param>
        /// <param name="propertyname"></param>
        /// <param name="propertyvalue"></param>
        private void UpdateNestedProperties(string parent, string fullname, string propertyname, string propertyvalue)
        {
            switch (parent)
            {
                case "(Relationship Matrix)":
                    if (propertyname == "CauseCodeId")
                    {
                        string[] matrix = new string[3];
                        matrix[0] = propertyvalue;
                        matrix[1] = GetNextMatrixValue("Classification");
                        matrix[2] = GetNextMatrixValue("Effect");
                        UpdateRelationshipMatrix(propertyname, fullname, matrix);
                    }
                    break;
            }
        }

        /// <summary>
        /// Update properties of nested items
        /// </summary>
        /// <param name="typename"></param>
        /// <param name="fullname"></param>
        /// <param name="propertyname"></param>
        /// <param name="propertyvalue"></param>
        private void UpdateAddNestedItem(string parent, string typename, string fullname)
        {
            switch (parent)
            {
                case "(Relationship Matrix)":

                    break;
            }
        }

        /// <summary>
        /// Get the object from fullname
        /// </summary>
        /// <param name="fname"></param>
        /// <returns></returns>
        private object GetCollectionObject(string fname)
        {
            string[] split = fname.ToString().Split(new Char[] { '|' });
            object o = project.AllItems[split[1]];
            object amplac = o.GetType().GetProperty(split[2]).GetValue(o, null);
            foreach (object cobject in (IEnumerable)amplac)
            {
                string name = cobject.GetType().GetProperty("Name").GetValue(cobject, null).ToString();
                if (name == split[3]) return null;
            }
            return amplac;
        }

        /// <summary>
        /// Add name period to existing named period collection
        /// </summary>
        /// <param name="typename"></param>
        /// <param name="pname"></param>
        /// <param name="fname"></param>
        private void UpdateAddCollectionItem(string typename, string fname)
        {
            string[] split = fname.ToString().Split(new Char[] { '|' });
            object amplac = GetCollectionObject(fname);
            Type t = CommonHelper.TypeHelper.GetType(typename);

            if (amplac == null) return;

            //collection 1
            NamedPeriodCollection npc = amplac as NamedPeriodCollection;
            if (npc != null)
            {
                NamedPeriod np = new NamedPeriod();
                np.Name = split[3];
                npc.Add(np);
                RecordImported(row);
                return;
            }
            //collection 2
            CustomViewFieldCollection cvfc = amplac as CustomViewFieldCollection;
            if (cvfc != null)
            {
                CustomViewField cvf = new CustomViewField();
                cvf.Name = split[3];
                cvfc.Add(cvf.Name);
                RecordImported(row);
                return;
            }
            //collection 1
            NamedInstanceCollection nic = amplac as NamedInstanceCollection;
            if (nic != null)
            {
                NamedInstance ni = new NamedInstance();
                ni.StartDateTimeUtc = DateTime.Now;
                ni.EndDateTimeUtc = DateTime.Now.AddDays(1);
                ni.Name = split[3];
                nic.Add(ni);
                RecordImported(row);
                return;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private string GetNextMatrixValue(string type)
        {
            DataRow datarow;

            switch (type)
            {
                case "Classification":
                    datarow = inputDataSet.Tables["Data"].Rows[row + 1];
                    if (datarow[2].ToString() == "ClassificationId")
                    {
                        return datarow[3].ToString();
                    }
                    break;
                case "Effect":
                    datarow = inputDataSet.Tables["Data"].Rows[row + 2];
                    if (datarow[2].ToString() == "EffectId")
                    {
                        return datarow[3].ToString();
                    }
                    break;
                default:
                    break;
            }
            return string.Empty;
        }

        /// <summary>
        ///Update collection properties
        /// </summary>
        /// <param name="tname"></param>
        /// <param name="pname"></param>
        /// <param name="fname"></param>
        /// <param name="pvalue"></param>
        private void UpdateCollection(string pname, string fname, string pvalue)
        {
            if (pname != null)
            {
                string[] split = fname.ToString().Split(new Char[] { '|' });
                object o = project.AllItems[split[1]];
                object amplac = o.GetType().GetProperty(split[2]).GetValue(o, null);
                foreach (object cobject in (IEnumerable)amplac)
                {
                    string name = cobject.GetType().GetProperty("Name").GetValue(cobject, null).ToString();
                    if (name == split[3])
                    {
                        PropertyInfo Pinfo = cobject.GetType().GetProperty(pname);
                        //if (Pinfo.PropertyType.Name == "DateTime") pvalue = System.Convert.ToDateTime(pvalue).ToUniversalTime().ToString();
                        UpdateProperty(Pinfo, pvalue, cobject, pname, fname);
                        return;
                    }
                }
                //AddToCollection(tname,fname,pvalue);
            }
        }

        /// <summary>
        /// Update Relationship Matrix
        /// </summary>
        /// <param name="tname"></param>
        /// <param name="pname"></param>
        /// <param name="fname"></param>
        /// <param name="pvalue"></param>
        private void UpdateRelationshipMatrix(string pname, string fname, string[] pvalue)
        {
            if (pname != null)
            {
                string[] split = fname.ToString().Split(new Char[] { '|' });
                object o = project.AllItems[split[1]];
                PropertyInfo pinfo = o.GetType().GetProperty(split[2]);
                object amplarm = pinfo.GetValue(o, null);

                EquipmentType et = o as EquipmentType;
                if (et != null)
                {
                    Matrix cmatrix = et.RelationshipMatrix.Clone();
                    Matrix dmatrix = et.EffectiveMatrix.Flatten();

                    for (int i = 0; i < cmatrix.Entries.Length; i++)
                    {
                        MatrixEntry me = (MatrixEntry)cmatrix.Entries[i];
                        string CauseCode = GetNameFromId(me.CauseCodeId, "CauseCode");
                        string Classification = GetNameFromId(me.ClassificationId, "Classification");
                        string effect = GetNameFromId(me.EffectId, "Effect");
                        if (pvalue[0] == CauseCode && pvalue[1] == Classification && pvalue[2] == effect)
                        {
                            return;
                        }
                    }
                    int newCauseCode = GetIdFromName(pvalue[0], "CauseCode");
                    int newClassification = GetIdFromName(pvalue[1], "Classification");
                    string newEffect = GetStringIdFromName(pvalue[2], "Effect");
                    //changed null to pvalue[2]
                    cmatrix.Add(new MatrixEntry(newCauseCode, newClassification, newEffect));
                    dmatrix.Add(new MatrixEntry(newCauseCode, newClassification, newEffect));
                    pinfo.SetValue(o, cmatrix, null);
                    pinfo.SetValue(o, dmatrix, null);
                    RecordImported(row);
                    RecordImported(row + 1);
                    RecordImported(row + 2);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        private void SetExtendedUserProperties(Item item, string fullname, string pname, string pvalue)
        {
            UserExtenderProvider user = new UserExtenderProvider();
            switch (pname)
            {
                case "AutoRefreshData":
                    user.SetAutoRefreshData(item, System.Convert.ToBoolean(pvalue));
                    RecordImported(row);
                    count++;
                    break;
                case "CanViewChart":
                    user.SetCanViewChart(item, System.Convert.ToBoolean(pvalue));
                    RecordImported(row);
                    count++;
                    break;
                case "CanViewFilter":
                    user.SetCanViewFilter(item, System.Convert.ToBoolean(pvalue));
                    RecordImported(row);
                    count++;
                    break;
                case "CanViewFolderSelector":
                    user.SetCanViewFolderSelector(item, System.Convert.ToBoolean(pvalue));
                    RecordImported(row);
                    count++;
                    break;
                case "CanViewGrid":
                    user.SetCanViewGrid(item, System.Convert.ToBoolean(pvalue));
                    RecordImported(row);
                    count++;
                    break;
                case "Hierarchies":
                    ComparableStringCollection cs = GetCSCString(pvalue);
                    user.SetHierarchies(item, cs);
                    RecordImported(row);
                    count++;
                    break;
                case "HistoryCache":
                    user.SetHistoryCache(item, Convert.ToInt32(pvalue));
                    RecordImported(row);
                    count++;
                    break;
                case "ShowFavoritesFolder":
                    user.SetShowFavoritesFolder(item, System.Convert.ToBoolean(pvalue));
                    RecordImported(row);
                    count++;
                    break;
                case "ShowHomeFolder":
                    user.SetShowHomeFolder(item, System.Convert.ToBoolean(pvalue));
                    RecordImported(row);
                    count++;
                    break;
                case "CanSaveFavorites":
                    user.SetCanSaveFavorites(item, System.Convert.ToBoolean(pvalue));
                    RecordImported(row);
                    count++;
                    break;
                case "CanSaveHome":
                    user.SetCanSaveHome(item, System.Convert.ToBoolean(pvalue));
                    RecordImported(row);
                    count++;
                    break;
            }
        }
        /// <summary>
        /// Update Ampla property from Excel row
        /// </summary>
        /// <param name="pinfo"></param>
        /// <param name="pvalue"></param>
        /// <param name="amplaobject"></param>
        /// <param name="pname"></param>
        private void UpdateProperty(PropertyInfo pinfo, string pvalue, object amplaobject, string pname, string fname)
        {
            if (pinfo != null)
            {
                if (amplaobject != null)
                {
                    //object op = (pvalue == string.Empty) ? null : GetPropertyValue(pinfo.PropertyType, pvalue,pname,fname);
                    object op = GetPropertyValue(pinfo.PropertyType, pvalue, pname, fname);
                    //if (op == null) return;
                    if (pinfo.CanWrite)
                    {
                        try
                        {
                            if (op.GetType() == Type.GetType("System.Guid") && (Guid)op == Guid.Empty)
                            {
                                RecordImported(row);
                            }
                            else
                            {
                                pinfo.SetValue(amplaobject, op, null);

                                RecordImported(row);
                            }
                            count++;
                        }
                        catch (Exception e)
                        {
                            mainLog.RaiseException(e);
                        }
                    }
                    else
                    {
                        if (!IsContained("IncludeList", pinfo.Name))
                        {
                            mainLog.WriteLine("Error: read only property");
                        }
                        else
                        {
                            RecordImported(row);
                            count++;
                        }
                    }
                }
                else
                {
                    mainLog.WriteLine("Error: could not resolve object name");
                }
            }
            else
            {
                mainLog.WriteLine(string.Concat("Error: could not resolve propertyname: ", pname));
            }
        }

        /// <summary>
        /// Change background color on successfull update
        /// </summary>
        /// <param name="backcolor"></param>
        /// <param name="forecolor"></param>
        private void FormatProcessed(int irow, int backcolor, int forecolor)
        {
            Excel.Range Row = (Excel.Range)Sheet.Rows[irow, Missing.Value];
            Row.Select();
            Row.Interior.ColorIndex = backcolor;
            Row.Font.ColorIndex = forecolor;

        }

        /// <summary>
        /// Cast the value to a given property
        /// </summary>
        /// <param name="type"></param>
        /// <param name="pv"></param>
        /// <returns></returns>
        private object GetPropertyValue(Type type, string pv, string name, string fullname)
        {
            if (type.IsEnum)
            {
                return StringToEnum(type, pv.ToString());
            }
            if (type.IsClass)
            {
                return GetClass(type, pv, name, fullname);
            }
            if (type.IsValueType)
            {
                if (type.Name == "TimeSpan")
                {
                    return GetTimeSpan(type, pv);
                }
                else if (type.Name == "Color")
                {
                    return GetColor(pv);
                }
                else if (type.Name == "NotificationSendOptions")
                {
                    return GetNotificationSendOptions(pv);
                }
                else if (type.Name == "Guid")
                {
                    return (pv == "00000000-0000-0000-0000-000000000000") ? Guid.Empty : new System.Guid(pv);
                }
                
                else
                {
                    if (type.Name == "DateTime") pv = System.Convert.ToDateTime(pv).ToUniversalTime().ToString();
                    return Convert.ChangeType(pv, type, null);
                }
            }
            return pv;
        }

        /// <summary>
        /// Returns the color object
        /// </summary>
        /// <param name="pv"></param>
        /// <returns></returns>
        private object GetColor(string pv)
        {
            int indexs = pv.IndexOf("[");
            int indexe = pv.IndexOf("]");
            string color = pv.Substring(indexs + 1, indexe - indexs - 1);
            string[] colorsplit = color.Split(new char[] { ',' });
            if (colorsplit.Length == 1)
            {
                return Color.FromName(color);
            }
            else
            {
                //argb
                if (colorsplit.Length == 4)
                {
                    int[] args = new int[4];
                    for (int i = 0; i < 4; i++)
                    {
                        int index = colorsplit[i].IndexOf("=") + 1;
                        int length = colorsplit[i].Length - index;
                        string scolor = colorsplit[i].Substring(index, length);
                        args[i] = Convert.ToInt32(scolor);
                    }
                    return Color.FromArgb(args[0], args[1], args[2], args[3]);
                }
            }
            return null;
        }

        private object getDependencyCollection(string pv, string fname, string pname)
        {
            object item = project.AllItems[fname].GetType().GetProperty(pname).GetValue(project.AllItems[fname], null);
            Item oitem = project.AllItems[fname] as Item;
            if (pv != string.Empty)
            {
                string[] split = pv.Split(new Char[] { ';' });
                Dependency[] dep = new Dependency[split.Length];
                for (int i = 0; i < split.Length; i++)
                {
                    string[] split1 = split[i].Split(new Char[] { ',' });
                    Item titem = project.AllItems[split1[0]] as Item;
                    ItemPropertyLink propLink = new ItemPropertyLink(oitem, titem, split1[1]);

                    switch (split1[2].ToString())
                    {
                        case "Trigger":
                            {
                                dep[i] = new Dependency(propLink, DependencyType.Trigger);
                                break;
                            }
                        case "Operand":
                            {
                                dep[i] = new Dependency(propLink, DependencyType.Operand);
                                break;
                            }
                        case "TriggerOperand":
                            {
                                dep[i] = new Dependency(propLink, DependencyType.TriggerOperand);
                                break;
                            }
                        case "Undefined":
                            {
                                dep[i] = new Dependency(propLink, DependencyType.Undefined);
                                break;
                            }
                    }
                }
                //test code
                DependencyCollection dc = new DependencyCollection(dep);
                //end test code
                return dc;
            }
            return new DependencyCollection();
        }
        /// <summary>
        /// Returns a stringset object
        /// </summary>
        /// <returns></returns>
        private object getStringSet(string pv, string fname, string pname)
        {
            object item = project.AllItems[fname].GetType().GetProperty(pname).GetValue(project.AllItems[fname], null);
            StringSet stringSet = (StringSet)item;
            StringSet stringSetReturn = (StringSet)stringSet.Clone();

            stringSetReturn.Entries.Clear();
            if (stringSetReturn.Entries.Count == 0)
            {
                //Set the string only for the current culture
                //stringSetReturn.Entries.Add(new DisplayLanguageInfo(DisplayLanguageInfo.CurrentDisplayLanguage.Parent.Culture),pv);
                stringSetReturn.Entries.Add(Citect.Common.Globalization.DisplayLanguageInfo.InvariantDisplayLanguage, pv);
                return stringSetReturn;
            }
            ////There should be only one entry in the collection for the current culture
            //foreach (DictionaryEntry key in stringSet.Entries)
            //{
            //    DisplayLanguageInfo display = (DisplayLanguageInfo)key.Key;
            //    if (DisplayLanguageInfo.CurrentDisplayLanguage.Parent.Culture.ToString() == display.Culture.ToString())
            //    {
            //        stringSetReturn.Entries.Add(key.Key, pv);
            //        return stringSetReturn;
            //    }
            //}

            return stringSetReturn;
        }

        /// <summary>
        /// Returns a class object
        /// </summary>
        /// <param name="type"></param>
        /// <param name="pv"></param>
        /// <returns></returns>
        private object GetClass(Type type, string pv, string pname, string fname)
        {
            switch (type.Name)
            {
                case "ReportParameters":
                    {

                        ReportParameterDescriptor desc = new ReportParameterDescriptor();
                        string[] split = pv.Split(new Char[] { ';' });
                        for (int i = 0; i < split.Length; i++)
                        {
                            string[] split1 = split[i].Split(new Char[] { '=' });
                            FieldDescriptor desc1 = new FieldDescriptor();
                            if (split1[0] != string.Empty)
                            {
                                desc1.Name = split1[0];
                                desc1.FieldValueName = split1[1];
                                desc.InputFormFieldDescriptors.Add(desc1);
                            }
                        }
                        if (desc.InputFormFieldDescriptors.Count > 0)
                        {
                            Citect.Ampla.General.Server.Reports.ReportParameters param = new Citect.Ampla.General.Server.Reports.ReportParameters(desc);
                            return param;
                        }
                        else
                        {
                            return null;
                        }
                    }
                case "ExpressionConfig":
                    {
                        Item oitem = project.AllItems[fname] as Item;
                        return ExpressionConfig.Create(oitem, pv);
                    }

                case "CultureInfo":
                    {
                        return ((pv != string.Empty) ? (new System.Globalization.CultureInfo(pv)) : null);
                    }
                case "User":
                    {
                        if (pv != string.Empty)
                        {
                            return project.AllItems[fname].GetType().GetProperty(pname).GetValue(project.AllItems[fname], null); ;
                        }
                        else
                        {
                            return null;
                        }
                    }
                case "String":
                    {
                        return Convert.ChangeType(pv, type);
                    }
                case "Item":
                case "ExtensibleItem":
                case "CustomView":
                case "CustomViewFieldCollection":
                case "NamedInstanceCollection":
                case "NamedPeriodCollection":
                case "SpecificationDescriptorCollection":
                case "Matrix":
                    {
                        return project.AllItems[fname].GetType().GetProperty(pname).GetValue(project.AllItems[fname], null);
                    }
                case "DependencyCollection":
                    {
                        return getDependencyCollection(pv, fname, pname);
                    }
                case "VariableBase":
                    {
                        return project.AllItems[pv];
                    }
                case "ComparableStringCollection":
                    {
                        return GetCSCString(pv);
                    }
                case "ItemLocationList":
                    {
                        return getItemLocationList(pv, fname, pname);
                    }
                case "StringCollection":
                    {
                        return GetSCString(pv);
                    }
                case "JoinFieldCollection":
                    {
                        return GetJFCString(pv);
                    }
                case "SubscriptionCollection":
                    {
                        return GetSCString(pv, fname, pname);
                    }
                case "PeriodHierarchy":
                    {
                        return GetHierarchy(pv, fname);
                    }
                case "HistoricalExpressionConfig":
                    {
                        return GetHistoricalExpression(pv, fname, pname);
                    }
                case "FunctionConfig":
                    {
                        return GetFunctionConfig(pv, fname, pname);
                    }
                case "OldExpression":
                case "Expression":
                case "ConditionExpression":
                    {
                        return GetExpression(pv, fname, pname);
                    }
                case "PercentValue":
                    {

                        return GetPercentValue(pv, type);
                    }
                case "Identity":
                    {
                        return ((pv != null)? ((Identity.GetSid(pv) != null) ? (new WindowsAccount(Identity.GetSid(pv))) : null) : null);
                    }
                case "FilterValues":
                    {
                        return GetFilterValues(pv);
                    }
                case "Type":
                    {
                        return Type.GetType("System." + pv);
                    }
                case "StringSet":
                    {
                        return getStringSet(pv, fname, pname);
                    }
                case "DateTime":
                    {
                        pv = System.Convert.ToDateTime(pv).ToUniversalTime().ToString();
                        return Convert.ChangeType(pv, type, null);
                    }
                case "OleDbCommand":
                    {
                        return GetOleDbCommand(pv, pname, fname);
                    }
                default:
                    {
                        return Convert.ChangeType(pv, type, null);
                    }
            }
        }
        /// <summary>
        /// It returns the OleDbCommand
        /// Parameters Collection not yet implemented
        /// </summary>
        /// <param name="pv"></param>
        /// <param name="pname"></param>
        /// <param name="fname"></param>
        /// <returns></returns>
        private object GetOleDbCommand(string pv, string pname, string fname)
        {
            string[] split = pv.Split(new Char[] { ',' });
            string commandParameters;
            string commandText = split[0].Split(new Char[] { ':' })[1].ToString();
            string commandTimeOut = split[1].Split(new Char[] { ':' })[1].ToString();
            if (split.Length >= 3)
            {
                commandParameters = split[2].Split(new char[] { ':' })[1].ToString();
            }
            else
            {
                commandParameters = string.Empty;
            }
            OleDbConnector.OleDbCommand command = (OleDbConnector.OleDbCommand)GetItem(pname, fname);
            command.CommandText = commandText;

            if (commandTimeOut != string.Empty) command.CommandTimeOut = System.Convert.ToInt32(commandTimeOut);

            if (commandParameters != string.Empty)
            {
                string[] parameters = commandParameters.Split(new char[] { ';' });

                command.Parameters.Clear();

                for (int counter = 0; counter < parameters.Length; counter++)
                {
                    string[] parameterProps = parameters[counter].Split(new char[] { '~' });
                    OleDbParameter parameter = new OleDbParameter();

                    if (parameterProps.Length >= 1)
                    {
                        if (parameterProps[0].ToString().Trim() != "")
                        {
                            parameter.ParameterName = parameterProps[0].ToString();

                            if (parameterProps.Length >= 2)
                            {
                                if (parameterProps[2].ToString().Trim() != "")
                                {
                                    switch (parameterProps[1].ToString().ToLower())
                                    {
                                        case "bigint":
                                            parameter.OleDbType = OleDbType.BigInt;
                                            break;
                                        case "binary":
                                            parameter.OleDbType = OleDbType.Binary;
                                            break;
                                        case "boolean":
                                            parameter.OleDbType = OleDbType.Boolean;
                                            break;
                                        case "bstr":
                                            parameter.OleDbType = OleDbType.BSTR;
                                            break;
                                        case "char":
                                            parameter.OleDbType = OleDbType.Char;
                                            break;
                                        case "currency":
                                            parameter.OleDbType = OleDbType.Currency;
                                            break;
                                        case "date":
                                            parameter.OleDbType = OleDbType.Date;
                                            break;
                                        case "dbdate":
                                            parameter.OleDbType = OleDbType.DBDate;
                                            break;
                                        case "dbtime":
                                            parameter.OleDbType = OleDbType.DBTime;
                                            break;
                                        case "dbtimestamp":
                                            parameter.OleDbType = OleDbType.DBTimeStamp;
                                            break;
                                        case "double":
                                            parameter.OleDbType = OleDbType.Double;
                                            break;
                                        case "empty":
                                            parameter.OleDbType = OleDbType.Empty;
                                            break;
                                        case "error":
                                            parameter.OleDbType = OleDbType.Error;
                                            break;
                                        case "filetime":
                                            parameter.OleDbType = OleDbType.Filetime;
                                            break;
                                        case "guid":
                                            parameter.OleDbType = OleDbType.Guid;
                                            break;
                                        case "idispatch":
                                            parameter.OleDbType = OleDbType.IDispatch;
                                            break;
                                        case "integer":
                                            parameter.OleDbType = OleDbType.Integer;
                                            break;
                                        case "iunknown":
                                            parameter.OleDbType = OleDbType.IUnknown;
                                            break;
                                        case "longvarbinary":
                                            parameter.OleDbType = OleDbType.LongVarBinary;
                                            break;
                                        case "longvarchar":
                                            parameter.OleDbType = OleDbType.LongVarChar;
                                            break;
                                        case "longvarwchar":
                                            parameter.OleDbType = OleDbType.LongVarWChar;
                                            break;
                                        case "numeric":
                                            parameter.OleDbType = OleDbType.Numeric;
                                            break;
                                        case "propvariant":
                                            parameter.OleDbType = OleDbType.PropVariant;
                                            break;
                                        case "single":
                                            parameter.OleDbType = OleDbType.Single;
                                            break;
                                        case "smallint":
                                            parameter.OleDbType = OleDbType.SmallInt;
                                            break;
                                        case "tinyint":
                                            parameter.OleDbType = OleDbType.TinyInt;
                                            break;
                                        case "unsignedbigint":
                                            parameter.OleDbType = OleDbType.UnsignedBigInt;
                                            break;
                                        case "unsignedint":
                                            parameter.OleDbType = OleDbType.UnsignedInt;
                                            break;
                                        case "unsignedsmallint":
                                            parameter.OleDbType = OleDbType.UnsignedSmallInt;
                                            break;
                                        case "unsignedtinyint":
                                            parameter.OleDbType = OleDbType.UnsignedTinyInt;
                                            break;
                                        case "varbinary":
                                            parameter.OleDbType = OleDbType.VarBinary;
                                            break;
                                        case "varchar":
                                            parameter.OleDbType = OleDbType.VarChar;
                                            break;
                                        case "variant":
                                            parameter.OleDbType = OleDbType.Variant;
                                            break;
                                        case "varnumeric":
                                            parameter.OleDbType = OleDbType.VarNumeric;
                                            break;
                                        case "varwchar":
                                            parameter.OleDbType = OleDbType.VarWChar;
                                            break;
                                        case "wchar":
                                            parameter.OleDbType = OleDbType.WChar;
                                            break;
                                    }
                                }
                            

                                if (parameterProps.Length >= 3)
                                {
                                    if (parameterProps[2].ToString().Trim() != "")
                                    {
                                        parameter.SourceColumn = parameterProps[2].ToString();
                                    }
                                }
                            }
                            command.Parameters.Add(parameter);
                        }
                        
                    }
                }
            }

            return command;
        }
        /// <summary>
        /// returns notification email send options
        /// </summary>
        /// <param name="pv"></param>
        /// <returns></returns>
        private object GetNotificationSendOptions(string pv)
        {
            string[] split = pv.Split(new Char[] { ',' });
            return new NotificationSendOptions(Convert.ToBoolean(split[1]), Convert.ToBoolean(split[0]));
        }
        /// <summary>
        /// Returns the filter collection
        /// </summary>
        /// <param name="pv"></param>
        /// <returns></returns>
        private object GetFilterValues(string pv)
        {
            string[] split = pv.Split(new string[] { "}," }, StringSplitOptions.None);
            Hashtable ht = new Hashtable();
            for (int i = 0; i < split.Length; i++)
            {
                if (!split[i].EndsWith("}")) split[i] = string.Concat(split[i], "}");
                string[] split1 = split[i].Split(new string[] { "=" }, StringSplitOptions.None);


                //string trim = split[i].Trim();
                //int index = trim.IndexOf("=");
                //string skey = trim.Substring(0, index);
                //string svalue = trim.Substring(index + 2, trim.Length - index - 3);
                ht.Add(split1[0].Trim(), split1[1].Trim(new char[] { '{' }).Trim(new char[] { '}' }));
            }
            return new FilterValues(ht);
        }
        /// <summary>
        /// Get the historicalexpresion and sets it
        /// </summary>
        /// <param name="pv"></param>
        /// <param name="fname"></param>
        /// <param name="pname"></param>
        /// <returns></returns>
        /// 

        private object GetHistoricalExpression(string pv, string fname, string pname)
        {
            //DG Start
            string expText = pv;
            string expTriggers = string.Empty;
            HistoricalExpressionConfig rhe;
            Citect.Ampla.Framework.ItemLinkSystem.ItemLinkCollection ilc;

            int expIndex = pv.IndexOf("Dependencies:");
            if (expIndex >= 0)
            {
                expText = pv.Substring(0, expIndex);
                int trigIndex = expIndex + 14;
                expTriggers = pv.Substring(trigIndex - 1);
            }

            //DG End
            object item = project.AllItems[fname].GetType().GetProperty(pname).GetValue(project.AllItems[fname], null);
            Item oitem = project.AllItems[fname] as Item;
            HistoricalExpressionConfig he = item as HistoricalExpressionConfig;

            //DG Start
            if (expTriggers != string.Empty)
            {
                string[] split = expTriggers.Split(new Char[] { ';' });
                Dependency[] dep = new Dependency[split.Length];
                ItemLink[] il = new ItemLink[split.Length];
                for (int i = 0; i < split.Length; i++)
                {
                    string[] split1 = split[i].Split(new Char[] { ',' });
                    Item titem = project.AllItems[split1[0]] as Item;
                    ItemPropertyLink propLink = new ItemPropertyLink(oitem, titem, split1[1]);
                    il[i] = new ItemLink(oitem, titem);

                    switch (split1[2].ToString())
                    {
                        case "Trigger":
                            {
                                dep[i] = new Dependency(propLink, DependencyType.Trigger);
                                break;
                            }
                        case "Operand":
                            {
                                dep[i] = new Dependency(propLink, DependencyType.Operand);
                                break;
                            }
                        case "TriggerOperand":
                            {
                                dep[i] = new Dependency(propLink, DependencyType.TriggerOperand);
                                break;
                            }
                        case "Undefined":
                            {
                                dep[i] = new Dependency(propLink, DependencyType.Undefined);
                                break;
                            }
                    }

                }
                DependencyCollection dc = new DependencyCollection(dep);
                ilc = new Citect.Ampla.Framework.ItemLinkSystem.ItemLinkCollection(il);
                //Citect.Ampla.Framework.Scripting.ExpressionConfig ep = new Citect.Ampla.Framework.Scripting.ExpressionConfig(expText, ilc, CompileAction.Compile);
                CodeCompileErrorCollection errors;
                Citect.Ampla.Framework.Scripting.ExpressionConfig ep = Citect.Ampla.Framework.Scripting.ExpressionConfig.Create(oitem, expText,out errors);
                rhe = new HistoricalExpressionConfig(ep, dc);

            }
            else
            {
                if (pv == null)
                {
                    pv = string.Empty;
                }
                Citect.Ampla.Framework.Scripting.ExpressionConfig ep = Citect.Ampla.Framework.Scripting.ExpressionConfig.Create(oitem, pv);
                DependencyCollection dc = new DependencyCollection();
                rhe = new HistoricalExpressionConfig(ep, dc);
            }
            //DG End

            return rhe;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pv"></param>
        /// <param name="fname"></param>
        /// <param name="pname"></param>
        /// <returns></returns>
        private object GetFunctionConfig(string pv, string fname, string pname)
        {
            Item oitem = project.AllItems[fname] as Item;
            return FunctionConfig.Create(oitem, pv);
        }
        /// <summary>
        /// Get the expresion and sets it
        /// </summary>
        /// <param name="pv"></param>
        /// <param name="fname"></param>
        /// <param name="pname"></param>
        /// <returns></returns>
        private object GetExpression(string pv, string fname, string pname)
        {
            string expText = pv;
            string expTriggers = string.Empty;

            int expIndex = pv.IndexOf("ExpressionTriggers:");
            if (expIndex >= 0)
            {
                expText = pv.Substring(0, expIndex);
                int trigIndex = expIndex + 20;
                expTriggers = pv.Substring(trigIndex - 1);
            }

            object item = project.AllItems[fname].GetType().GetProperty(pname).GetValue(project.AllItems[fname], null);
            if (expTriggers != string.Empty)
            {
                //Expression exp = item as Expression;
                //SubscriptionCollection c = (SubscriptionCollection)exp.Subscriptions;
                //c.Clear();
                //string[] split = expTriggers.Split(new Char[] { ';' });
                //for(int i = 0; i < split.Length; i++)
                //{
                //    string[] split1 = split[i].Split(new Char[] { ',' });	
                //    c.Add(split1[0],split1[1]);
                //}
            }
            PropertyInfo pinfotext = item.GetType().GetProperty("Text");
            PropertyInfo pinfocompile = item.GetType().GetProperty("CompileAction");
            pinfotext.SetValue(item, expText, null);
            pinfocompile.SetValue(item, this.StringToEnum(pinfocompile.PropertyType, "Compile"), null);
            return item;
        }

        /// <summary>
        /// Get ComparableStringCollection
        /// </summary>
        /// <param name="pv"></param>
        /// <returns></returns>
        private ComparableStringCollection GetCSCString(string pv)
        {
            if (pv.IndexOf(",") > -1)
            {
                string[] split = pv.Split(new Char[] { ',' });
                ComparableStringCollection csc = new ComparableStringCollection();
                for (int i = 0; i < split.Length; i++) csc.Add(split[i]);
                return csc;
            }
            else
            {

                ComparableStringCollection csc = new ComparableStringCollection();
                if (pv != string.Empty) csc.Add(pv);
                return csc;
            }
        }
        /// <summary>
        /// Get ComparableStringCollection
        /// </summary>
        /// <param name="pv"></param>
        /// <returns></returns>
        private ItemLocationList getItemLocationList(string pv, string fname, string pname)
        {
            if (pv == string.Empty) return ItemLocationList.Empty;
            Item item = project.AllItems[fname] as Item;

            if (pv.IndexOf(",") > -1)
            {
                string[] split = pv.Split(new Char[] { ',' });
                
                Item[] items = new Item[split.Length];
                for (int i = 0; i < split.Length; i++) items[i] = project.AllItems[split[i]];

                return ItemLocationList.Create(item,items);
            }
            else
            {
                Item items = project.AllItems[pv] as Item;
                return ItemLocationList.Create(item,items);
            }
        }
        /// <summary>
        /// Get StringCollection
        /// </summary>
        /// <param name="pv"></param>
        /// <returns></returns>
        private StringCollection GetSCString(string pv)
        {
            if (pv.IndexOf(",") > -1)
            {
                string[] split = pv.Split(new Char[] { ',' });
                StringCollection sc = new StringCollection();
                for (int i = 0; i < split.Length; i++) sc.Add(split[i]);
                return sc;
            }
            else
            {
                StringCollection sc = new StringCollection();
                if (pv != string.Empty) sc.Add(pv);
                return sc;
            }
        }
        /// <summary>
        /// Get JoinFieldCollection
        /// </summary>
        /// <param name="pv"></param>
        /// <returns></returns>
        private JoinFieldCollection GetJFCString(string pv)
        {
            string[] split = pv.Split(new Char[] { ';' });
            JoinFieldCollection csc = new JoinFieldCollection();
            for (int i = 0; i < split.Length; i++)
            {
                string[] split1 = split[i].Split(new Char[] { ',' });
                csc.Add(split1[0], split1[1]);
            }
            return csc;
        }
        /// <summary>
        /// Get SubscriptionCollection
        /// </summary>
        /// <param name="pv"></param>
        /// <returns></returns>
        private SubscriptionCollection GetSCString(string pv, string fname, string pname)
        {
            object item = project.AllItems[fname].GetType().GetProperty(pname).GetValue(project.AllItems[fname], null);
            string[] split = pv.Split(new Char[] { ';' });
            SubscriptionCollection csc = item as SubscriptionCollection;
            csc.Clear();
            for (int i = 0; i < split.Length; i++)
            {
                string[] split1 = split[i].Split(new Char[] { ',' });
                csc.Add(new Subscription(split1[0], split1[1]));
            }
            return csc;
        }
        /// <summary>
        /// GetHierarchy
        /// </summary>
        /// <param name="pv"></param>
        /// <returns></returns>
        private object GetHierarchy(string pv, string fname)
        {
            if (pv == null || pv.Trim() == "")
            {
                PeriodHierarchy ph = new PeriodHierarchy();
                return ph;
            }
            if (pv.IndexOf("Inherited") == 0)
            {
                return null;
            }
            else
            {
                string[] split = pv.Split(new Char[] { ',' });
                PeriodHierarchy ph = new PeriodHierarchy();
                for (int i = 0; i < split.Length; i++)
                {
                    split[i] = split[i].Trim();
                    ph.Add(split[i]);
                }
                return ph;
            }
        }

        /// <summary>
        /// not used
        /// </summary>
        /// <param name="name"></param>
        /// <param name="inherited"></param>
        /// <returns></returns>
        private PeriodHierarchy GetPeriod(string name, string inherited)
        {
            string[] split = name.Split(new Char[] { ',' });
            PeriodHierarchy ph = new PeriodHierarchy();
            for (int i = 0; i < split.Length; i++)
            {
                split[i] = split[i].Trim();
                ph.Add(split[i]);
            }
            if (inherited == string.Empty)
            {
                return ph;
            }
            else
            {
                return new PeriodHierarchy(ph, inherited);
            }
        }

        /// <summary>
        /// Returns the modified collection
        /// </summary>
        /// <param name="pname"></param>
        /// <param name="fname"></param>
        /// <returns></returns>
        private object GetItem(string pname, string fname)
        {
            foreach (PropertyInfo pinfo in project.AllItems[fname].GetType().GetProperties())
            {
                if (pinfo.Name == pname)
                {
                    return pinfo.GetValue(project.AllItems[fname], null);
                }
            }
            return null;
        }
        /// <summary>
        /// returns a time span objectPeriodHierarchyEditor
        /// </summary>
        /// <param name="type"></param>
        /// <param name="pv"></param>
        /// <returns></returns>
        private object GetTimeSpan(Type type, string pv)
        {
            Decimal ticks = 0;
            System.TimeSpan ts;
            int hours = 0;
            int minutes = 0;
            int seconds = 0;

            string[] PV = pv.ToString().Split(new Char[] { ':' });

            if (PV[0].IndexOf(".") != -1 || PV[1].IndexOf(".") != -1 || PV[2].IndexOf(".") != -1)
            {
                ticks = Convert.ToDecimal(TimeSpan.TicksPerHour) * Convert.ToDecimal(PV[0]);
                ticks += Convert.ToDecimal(TimeSpan.TicksPerMinute) * Convert.ToDecimal(PV[1]);
                ticks += Convert.ToDecimal(TimeSpan.TicksPerSecond) * Convert.ToDecimal(PV[2]);
                ts = new TimeSpan((long)ticks);
            }
            else
            {
                hours = Convert.ToInt32(PV[0]);
                minutes = Convert.ToInt32(PV[1]);
                seconds = Convert.ToInt32(PV[2]);
                ts = new TimeSpan(hours, minutes, seconds);
            }
            return ts;
        }
        /// <summary>
        /// Gets the value of the property
        /// </summary>
        /// <param name="propertyvalue"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private string GetValue(object propertyvalue, Type type, Item item)
        {
            if (type.Name == "OleDbCommand")
            {
                OleDbConnector.OleDbCommand command = propertyvalue as OleDbConnector.OleDbCommand;
                string text = "CommandText:" + command.CommandText.ToString();
                string timeout = "CommandTimeOut:" + command.CommandTimeOut.ToString();
                string parameters = "CommandParameters:";
                for (int counter = 0; counter < command.Parameters.Count; counter++)
                {
                    string parameterProps = command.Parameters[counter].ParameterName.ToString() + '~' +
                                            command.Parameters[counter].OleDbType.ToString() + '~' + 
                                            command.Parameters[counter].SourceColumn.ToString();

                     parameters = parameters + parameterProps + ";";
                }
                if (parameters[parameters.Length-1] == ';')
                {
                    parameters = parameters.Remove(parameters.Length - 1);
                }

                return string.Concat(text,",",timeout, ",", parameters);
                
            }
            if (type.Name == "StringSet")
            {
                StringSet stringSet = (StringSet)propertyvalue;
                //There should only be one or none entries
                if (stringSet.Entries.Count > 0)
                {
                    foreach (DictionaryEntry key in stringSet.Entries)
                    {
                        return key.Value.ToString();
                    }
                }
                else
                {
                    //This is trricky as the current culture sometimes get set to invariant
                    foreach(DictionaryEntry entry in stringSet.Resources)
                    {
                        DisplayLanguageInfo languageInfo = (DisplayLanguageInfo) entry.Key;
                        if (languageInfo.Culture.TwoLetterISOLanguageName == DisplayLanguageInfo.CurrentDisplayLanguage.Culture.TwoLetterISOLanguageName
                             || languageInfo.Culture.TwoLetterISOLanguageName == "iv")
                        {
                            return entry.Value.ToString();
                        }
                        
                    }

                }
                return string.Empty;
            }
            if (type.Name == "NotificationSendOptions")
            {
                NotificationSendOptions options = (NotificationSendOptions)propertyvalue;
                return options.OutsideCalendarPeriod.ToString() + "," + options.WithinCalendarPeriod.ToString();
            }
            if (type.Name == "ComparableStringCollection")
            {
                ComparableStringCollection c = (ComparableStringCollection)propertyvalue;
                string[] cs = new string[c.Count];
                for (int i = 0; i < c.Count; i++) cs[i] = c[i];
                return string.Join(",", cs);
            }
            if (type.Name == "StringCollection")
            {
                StringCollection c = (StringCollection)propertyvalue;
                string[] cs = new string[c.Count];
                for (int i = 0; i < c.Count; i++) cs[i] = c[i];
                return string.Join(",", cs);
            }
            if (type.Name == "StringSet")
            {
                Citect.Common.Globalization.StringSet s = (Citect.Common.Globalization.StringSet)propertyvalue;
                return s.ToString();
            }
            if (type.Name == "JoinFieldCollection")
            {
                JoinFieldCollection c = (JoinFieldCollection)propertyvalue;
                string[] cs = new string[c.Count];
                for (int i = 0; i < c.Count; i++) cs[i] = c[i].PrimaryField + "," + c[i].SecondaryField;
                return string.Join(";", cs);
            }
            if (type.Name == "FunctionConfig")
            {
                FunctionConfig fc = (FunctionConfig)propertyvalue;
                return fc.GetDisplayText(item);
            }
            if (type.Name == "SubscriptionCollection")
            {
                SubscriptionCollection c = (SubscriptionCollection)propertyvalue;
                string[] cs = new string[c.Count];
                for (int i = 0; i < c.Count; i++) cs[i] = c[i].ItemName + "," + c[i].EventName;
                return string.Join(";", cs);
            }
            //DG Start

            //TypeDescriptor.GetConverter(myGuid).ConvertFrom(myGuidString)

            else if (type.Name == "DependencyCollection")
            {
                DependencyCollection dc = (DependencyCollection)propertyvalue;
                string[] dep = new string[dc.Count];
                for (int i = 0; i < dc.Count; i++)
                {
                    dep[i] = dc[i].PropertyLink.ItemLink.AbsolutePath + "," + dc[i].PropertyLink.PropertyName + "," + dc[i].DependencyType;
                }
                return string.Join(";", dep);
            }
            //DG End
            else if (type.Name == "HistoricalExpressionConfig")
            {
                HistoricalExpressionConfig hec = propertyvalue as HistoricalExpressionConfig;
                if (hec != null)
                {
                    //string ret = hec.GetDisplayText(item).Trim();
                    //string ret = hec.GetCompileText(item).Trim();
                    string ret = hec.GetDisplayText(item).Trim();
                    //string ret = hec.Format;
                    string[] dep = new string[hec.Dependencies.Count];
                    DependencyCollection dc = (DependencyCollection)hec.Dependencies;

                    //DG Start
                    for (int i = 0; i < hec.Dependencies.Count; i++)
                    {
                        dep[i] = dc[i].PropertyLink.ItemLink.AbsolutePath + "," + dc[i].PropertyLink.PropertyName + "," + dc[i].DependencyType.ToString();
                    }
                    if (dep.Length > 0)
                    {
                        return ret + "Dependencies:" + string.Join(";", dep);
                    }
                    else
                    {
                        return ret;
                    }
                    //DG End
                }
                return string.Empty;
            }
            if (type.Name == "Expression")
            {
                return string.Empty;
            }
            if (type.Name == "ExpressionConfig")
            {
                ExpressionConfig expression = propertyvalue as ExpressionConfig;
                if (expression != null) return expression.GetDisplayText(item).Trim();
                return string.Empty;
            }
            else if (type.Name == "Identity")
            {
                if (propertyvalue != null) return ((WindowsAccount)propertyvalue).Name;
                return string.Empty;
            }
            else if (type.Name == "Item" || type.Name == "ExtensibleItem" || type.Name == "VariableBase")
            {
                Item np = propertyvalue as Item;
                if (np != null) return np.FullName;
                return string.Empty;
            }
            else if (type.Name == "Type")
            {
                Type pv = propertyvalue as Type;
                return pv.Name;
            }
            else
            {
                if (propertyvalue != null && propertyvalue.ToString() != "")
                {
                    if (type.Name == "DateTime") propertyvalue = System.Convert.ToDateTime(propertyvalue).ToLocalTime().ToString();
                    object oc = Convert.ChangeType(propertyvalue, type);
                    return oc.ToString();
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// Convert any possible string-Value of a given enumeration
        /// type to its internal representation.
        /// </summary>
        /// <param name="t"></param>
        /// <param name="Value"></param>
        /// <returns></returns>
        private object StringToEnum(Type t, string Value)
        {
            foreach (FieldInfo fi in t.GetFields())
                if (fi.Name == Value) return fi.GetValue(null);
            // We use null because
            // enumeration values
            // are static

            throw new Exception(string.Format("Can't convert {0} to {1}", Value, t.ToString()));
        }

        /// <summary>
        /// Convert string to Metrics PercentValue
        /// </summary>
        /// <param name="type"></param>
        /// <param name="Value"></param>
        /// <returns></returns>
        private object GetPercentValue(string Value, Type type)
        {
            Type[] types = new Type[1];
            types[0] = typeof(string);

            ConstructorInfo ci = type.GetConstructor(types);
            object[] consargs = new object[1];
            consargs[0] = Value;

            return ci.Invoke(consargs);
        }
        #endregion //Import

        #region Add methods--------------------------------------------------------------------------------

        /// <summary>
        /// Get Ampla type from fullname;
        /// </summary>
        /// <param name="oitem"></param>
        /// <param name="typename"></param>
        /// <param name="fullname"></param>
        /// <returns></returns>
        private Type GetAmplaType(string typename)
        {
            //oitem = project.AllItems[fullname];
            //return project.AllItems[fullname].GetType();
            //Type type = CommonHelper.TypeHelper.GetType(typename);
            Type type = GetType(typename);
            return type;
        }

        /// <summary>
        /// Gets a Type for the specified class from the specified assemlby, if that assembly
        /// is already loaded into the AppDomain.
        /// </summary>
        /// <param name="assemblyName">The assembly to find.</param>
        /// <param name="className">The class to get the Type for.</param>
        /// <returns>
        ///	A Type if the assembly and class could be found, otherwise null.
        /// </returns>
        private static Type getTypeFromLoadedAssembly(string assemblyName, string className)
        {
            Type type = null;

            Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
            string loadedAssembly;

            for (int i = 0; i < assemblies.Length; i++)
            {
                loadedAssembly = assemblies[i].GetName().Name;
                if (loadedAssembly == assemblyName)
                {
                    type = assemblies[i].GetType(className);
                    break;
                }
            }

            return type;
        }

        /// <summary>
        ///	Attempts to load a Type object from a string that identifies the class name.
        /// </summary>
        /// <param name="className">The type to load.</param>
        /// <returns>
        ///	A Type object, or null if the specified type could not be found.
        /// </returns>
        /// <remarks>
        ///	If the class cannot be loaded using the provided string, then a repetative
        ///	attempt is made to find the type in a specific assembly, where that assembly
        ///	is named as a prefix of the type name.
        ///	That is...if the class name is Citect.Ampla.General.Server.Helper.SerializationHelper, then
        ///	it will try to load the type from Citect.P2Bapps.Server.Helper first. Assuming this
        ///	fails, it would then try Citect.Ampla.General.Server. And so on, until either the string
        ///	is exhausted, or a type is returned.
        /// </remarks>
        public static Type GetType(string className)
        {
            Type type = Type.GetType(className);

            string assemblyName = className;
            string typeString = null;

            while ((type == null) && (assemblyName != String.Empty))
            {
                int i = assemblyName.LastIndexOf('.');
                if (i > 0)
                {
                    assemblyName = assemblyName.Substring(0, i);
                    typeString = className + "," + assemblyName; 
                    type = Type.GetType(typeString);

                    if (type == null)
                    {
                        if (assemblyName.IndexOf("Isa95") > 0) assemblyName = "Citect.Ampla.General.Server";
                        type = getTypeFromLoadedAssembly(assemblyName, className);
                    }
                }
                else
                {
                    break;
                }
            }
            return type;
        }

        /// <summary>
        /// Add new Ampla item
        /// </summary>
        /// <param name="typename"></param>
        /// <param name="fullname"></param>
        /// <returns></returns>
        private Item AddItem(string typename, string fullname)
        {
            ChildItemsCollection items = null;
            Type AmplaType;
            int lastDot = -1;
            string item;
            
            AmplaType = GetAmplaType(typename);
            lastDot = fullname.LastIndexOf('.');
            if (-1 == lastDot)
            {
                item = fullname;
                items = project.RootItems;
            }
            else
            {
                string parent = fullname.Substring(0, lastDot);
                item = fullname.Substring(lastDot + 1, fullname.Length - lastDot - 1);
                items = project.AllItems[parent].Items;
            }
            
            return items.AddNew(AmplaType, item);
        }

        #endregion //Add

        #region Delete methods-----------------------------------------------------------------------------


        #endregion //Delete

        #region Excel methods------------------------------------------------------------------------------

        /// <summary>
        /// Open Excel file
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        private bool OpenExcel(string filename)
        {
            App = new Excel.Application();
            string workbookPath = filename;

            try
            {
                FileInfo file = new FileInfo(filename);
                if (file.Exists)
                {
                    if (this.import)
                    {
                        if (!IsFileOpen(filename))
                        {
                            Workbook = App.Workbooks.Open(workbookPath,
                                0, false, 5, "", "", false, Excel.XlPlatform.xlWindows, "",
                                true, false, 0, true, false, false);
                            mainLog.WriteLine("Excel file open");
                            return true;
                        }
                        else
                        {
                            MessageBox.Show(string.Concat("Please close the import Excel file: ", filename), "File Already Open");
                            return false;
                        }
                    }
                    if (this.export)
                    {
                        file.Delete();
                    }
                }
            }
            catch (Exception ex)
            {
                mainLog.RaiseException(ex);
                mainLog.CloseFile();
                return false;
            }
            return false;
        }

        /// <summary>
        /// Check if Excel file is already open
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        private bool IsFileOpen(string file)
        {
            //This thing needs more work, Microsoft does not cleanly let you know that the file is open,
            //so you have to make it bounce and then check up the exception.... f#@&!
            try
            {
                FileStream fileObj = new FileStream(file, FileMode.Open, FileAccess.ReadWrite, FileShare.None);
                fileObj.Close();
                return false;
            }
            catch (UnauthorizedAccessException ex)
            {
                mainLog.RaiseException(ex);
                return true;
            }
            catch (Exception ex)
            {
                mainLog.RaiseException(ex);
                return true;
            }
        }

        /// <summary>
        /// Select worksheet Sheet
        /// </summary>
        /// <returns></returns>
        private bool SelectSheet(string sheet)
        {
            Sheets = Workbook.Worksheets;
            Sheet = (Excel.Worksheet)Sheets.get_Item(sheet);
            if (Sheet == null)
            {
                mainLog.WriteLine("Error: Could not select Import worksheet");
                //MessageBox.Show(mainform, "Error: Could not select Import worksheet");
                options.ShowMessage("Error: Could not select Import worksheet");
                return false;
            }
            mainLog.WriteLine("Worksheet Import selected");
            return true;
        }

        /// <summary>
        /// Validate open excel file
        /// </summary>
        /// <returns>Success or failure</returns>
        private bool ValidateFile()
        {
            int sheetCounter = 0;

            foreach (Excel.Worksheet sheet in Workbook.Worksheets)
            {
                Regex sheetNamePattern = new Regex(string.Format("{0}[0-9][0-9]", importSheet));

                if (sheet.Name == importSheet || (sheet.Name.IndexOf(importSheet) >= 0 && sheetNamePattern.IsMatch(sheet.Name)))
                {
                    Sheet = sheet;
                    colTypeName = GetColumn(hTypename);
                    colFullName = GetColumn(hFullname);
                    colPropertyName = GetColumn(hPropertyname);
                    colPropertyValue = GetColumn(hPropertyvalue);
                    if (colTypeName == -1 || colTypeName == -1 || colTypeName == -1 || colTypeName == -1)
                    {
                        mainLog.WriteLine("Error: Excel configuration file did not validated,column not found");
                        //MessageBox.Show(mainform, "Error: Excel configuration file did not validated,column not found");
                        options.ShowMessage("Error: Excel configuration file did not validated,column not found");
                        return false;
                    }
                    sheetCounter++;
                }
            }
            if (sheetCounter > 0)
            {
                mainLog.WriteLine("Excel configuration file validated");
                return true;
            }
            else
            {
                mainLog.WriteLine("Error: Excel configuration file did not validated,column not found");
                //MessageBox.Show(mainform, "Error: Excel configuration file did not validated,column not found");
                options.ShowMessage("Error: Excel configuration file did not validated,Ampla sheet(s) not found");
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private bool ValidateSheet(Excel.Worksheet sheet)
        {
            Regex sheetNamePattern = new Regex(string.Format("{0}[0-9][0-9]", importSheet));

            if (!(sheet.Name == importSheet || (sheet.Name.IndexOf(importSheet) >= 0 && sheetNamePattern.IsMatch(sheet.Name))))
            {
                return false;
            }
            SelectSheet(sheet.Name);
            colTypeName = GetColumn(hTypename);
            colFullName = GetColumn(hFullname);
            colPropertyName = GetColumn(hPropertyname);
            colPropertyValue = GetColumn(hPropertyvalue);
            if (colTypeName == -1 || colTypeName == -1 || colTypeName == -1 || colTypeName == -1)
            {
                mainLog.WriteLine("Error: Excel configuration file did not validated,column not found");
                //MessageBox.Show(mainform, "Error: Excel configuration file did not validated,column not found");
                //options.ShowMessage("Error: Excel configuration file did not validated,column not found");
                return false;
            }
            return true;
        }


        /// <summary>
        /// Get column index given the column name
        /// </summary>
        /// <param name="columnname"></param>
        /// <returns></returns>
        private int GetColumn(string columnname)
        {
            Excel.Range Cell;

            try
            {
                Sheet.Select(Missing.Value);
                for (int index = 1; index <= maxColumns; index++)
                {
                    Cell = (Excel.Range)Sheet.Cells[1, index];
                    if (Cell.Value2.ToString().Equals(columnname))
                    {
                        return Cell.Column;
                    }
                }
            }
            catch (Exception e)
            {
                mainLog.WriteLine(e.Message);
                mainLog.CloseFile();
            }
            return -1;
        }

        /// <summary>
        /// Get cell value as string
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <param name="columnIndex"></param>
        /// <returns></returns>
        private string getCellString(int rowIndex, int columnIndex)
        {
            object cellValue = getCellValue(rowIndex, columnIndex);
            int maxCellSize = (int.TryParse(GetKeyValue("ExcelMaxCellSize"), out maxCellSize)) ? maxCellSize : 32000;

            if (cellValue != null && cellValue.ToString().Length >= maxCellSize)
            {
                string returnValue = cellValue.ToString();
                object nextCellValue = getCellValue(rowIndex, columnIndex + 1);
                int counter = 1;

                while (nextCellValue != null && nextCellValue.ToString().Length > 0)
                {
                    string value = nextCellValue.ToString();
                    if (value.Substring(0, 1) == EXCEL_CHAR_REPLACEBY)
                    {
                        value = string.Concat(EXCEL_CHAR_TOREPLACE, value.Substring(1));
                    }
                    returnValue = string.Concat(returnValue, value);

                    counter++;
                    nextCellValue = getCellValue(rowIndex, columnIndex + counter);

                }
                return returnValue;
            }
            else
            {
                return (cellValue == null) ? null : cellValue.ToString();
            }
        }

        /// <summary>
        /// Check if Excel cell is empty
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <param name="columnIndex"></param>
        /// <returns></returns>
        private bool isCellEmpty(int rowIndex, int columnIndex)
        {
            object cellContents = getCellValue(rowIndex, columnIndex);
            if (cellContents == null || cellContents.ToString().Length == 0)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Check if Excel row has been edited
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool isRowEdited(int rowIndex)
        {
            //if (!isRowOK(rowIndex)) return false;
            Excel.Range row = (Excel.Range)Sheet.Rows[rowIndex, Missing.Value];
            row.Select();
            if ((int)row.Interior.ColorIndex == 1 && (int)row.Font.ColorIndex == 2) return true;
            //Pattern = xlSolid
            return false;
        }

        /// <summary>
        /// Check if row is OK
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool isRowOK(int rowIndex)
        {
            // Check Item FullName.
            string itemFullName = getCellString(rowIndex, colFullName);
            if (itemFullName == null || itemFullName.Length == 0)
            {
                return false;
            }

            // Check Item Type.
            string itemType = getCellString(rowIndex, colTypeName);
            if (itemType == null || itemType.Length == 0)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Get cell value as object
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <param name="columnIndex"></param>
        /// <returns></returns>
        private object getCellValue(int rowIndex, int columnIndex)
        {
            if (columnIndex >= maxColumns) throw new ArgumentException("Column is greater than maximum columns", "columnIndex");
            return ((Excel.Range)Sheet.Cells[rowIndex, columnIndex]).Value2;
        }

        /// <summary>
        /// Set value of Excel cell
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <param name="columnIndex"></param>
        /// <param name="cellValue"></param>
        private void setCellValue(int rowIndex, int columnIndex, object cellValue)
        {
            int maxCellSize = (int.TryParse(GetKeyValue("ExcelMaxCellSize"), out maxCellSize)) ? maxCellSize : 32000;
            if (columnIndex >= maxColumns) throw new ArgumentException("Column is greater than maximum columns", "columnIndex");

            string cellString = null;
            if (cellValue != null)
            {
                cellString = cellValue.ToString();
            }

            Excel.Range cell = ((Excel.Range)Sheet.Cells[rowIndex, columnIndex]);
            if (columnIndex > intrinsicColumnCount)
            {
                if (stringContainsNumber(cellString))
                {
                    cellString = "'" + cellString;
                }

                cell.Value2 = cellString;
                cell.NumberFormat = textFormatSpecifier;
            }
            else
            {
                int cellLen = cellValue.ToString().Length;
                int cellStringLen = cellString.Length;

                if (cellLen >= maxCellSize)
                {
                    int numberOfBlocks = (cellLen % maxCellSize == 0) ? (cellLen / maxCellSize) : (cellLen / maxCellSize + 1);
                    for (int i = 0; i < numberOfBlocks; i++)
                    {
                        int blockLen = (numberOfBlocks == (i + 1)) ? (cellLen - (numberOfBlocks - 1) * maxCellSize) : maxCellSize;
                        string blockValue = cellString.Substring(i * maxCellSize, blockLen);
                        if (blockValue.Substring(0, 1) == EXCEL_CHAR_TOREPLACE) blockValue = string.Concat(EXCEL_CHAR_REPLACEBY, blockValue.Substring(1));
                        int test = blockValue.Length;
                        Excel.Range blockCell = ((Excel.Range)Sheet.Cells[rowIndex, columnIndex + i]);
                        cell.NumberFormat = textFormatSpecifier;
                        blockCell.Value2 = blockValue;
                        //Thread.Sleep(100);
                    }
                }
                else
                {
                    cell.NumberFormat = textFormatSpecifier;
                    cell.Value2 = cellValue;
                }
            }

            //cell.Interior.ColorIndex = Excel.XlColorIndex.xlColorIndexNone;
        }

        /// <summary>
        /// Enter column titles
        /// </summary>
        private void populateColumnHeader()
        {
            setCellValue(1, 1, hTypename);
            setCellValue(1, 2, hFullname);
            setCellValue(1, 3, hPropertyname);
            setCellValue(1, 4, hPropertyvalue);
        }

        /// <summary>
        /// Format the PropertyValue Excel column so values are text
        /// </summary>
        private void formatPropertyValueColumn(string sheet)
        {
            if (SelectSheet(sheet))
            {
                Excel.Range Col = (Excel.Range)Sheet.Cells[Missing.Value, GetColumn(hPropertyvalue)];
                Col.EntireColumn.Select();
                Col.EntireColumn.NumberFormat = "@";
            }
        }
        /// <summary>
        /// Select Excel sheet given its name
        /// </summary>
        /// <param name="sheetname"></param>
        /// <returns></returns>
        private Excel.Worksheet GetSheet(string sheetname)
        {
            foreach (Excel.Worksheet ws in Workbook.Sheets)
            {
                if (ws.Name == sheetname)
                {
                    ws.Select(Missing.Value);
                    return ws;
                }
            }
            return null;
        }

        /// <summary>
        /// Test if string contains a number
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private bool stringContainsNumber(string value)
        {
            if (value == null || value.Length == 0)
            {
                return false;
            }

            bool result = true;

            foreach (char c in value)
            {
                if (!Char.IsWhiteSpace(c) &&
                    !Char.IsNumber(c) &&
                    !Char.IsSymbol(c) &&
                    !Char.IsSeparator(c) &&
                    c != ':' &&
                    c != '.' &&
                    c != '%')
                {
                    result = false;
                    break;
                }
            }
            return result;
        }

        /// <summary>
        /// Gets or create Excel file
        /// </summary>
        /// <param name="workbookname"></param>
        /// <returns></returns>
        private bool GetExcelWorkbook(string workbookname, string sheetName)
        {
            try
            {
                if (!this.OpenExcel(workbookname))
                {
                    this.Workbook = App.Workbooks.Add(Missing.Value);
                    this.Sheet = (Excel.Worksheet)this.Workbook.Sheets[1];
                    this.Sheet.Name = sheetName;
                    this.Sheet.Cells.Clear();
                }
                else
                {
                    foreach (Excel.Worksheet s in this.Workbook.Sheets)
                    {
                        if (s.Name == exportSheet)
                        {
                            this.Sheet = s;
                            this.Sheet.Select(Missing.Value);
                            return (Workbook != null);
                        }
                    }
                    this.Sheet = (Excel.Worksheet)Workbook.Sheets.Add(Missing.Value,
                        Missing.Value, Missing.Value, Missing.Value);
                    this.Sheet.Name = sheetName;
                    this.Sheet.Select(Missing.Value);
                }
                return (Workbook != null);
            }
            catch (Exception ex)
            {
                mainLog.RaiseException(ex);
                mainLog.CloseFile();
                return false;
            }
        }

        /// <summary>
        /// Format Ampla items
        private void FormatItemRow(int rownumber)
        {
            Excel.Range row = (Excel.Range)Sheet.Rows[rownumber, Missing.Value];
            row.Font.Italic = true;
            row.Font.Bold = true;
        }

        /// <summary>
        /// Formats Excel export file top row
        /// </summary>
        private void FormatTopRow()
        {
            Excel.Range row = (Excel.Range)Sheet.Rows[1, Missing.Value];
            row.Font.Italic = true;
            row.Font.Bold = true;
            row.Font.Size = 12;
            Excel.Range cell = ((Excel.Range)Sheet.Cells[2, 1]);
            cell.Select();
            App.ActiveWindow.FreezePanes = true;
        }

        /// <summary>
        /// Format Excel export file
        /// </summary>
        private void FormatAutoFit()
        {
            Excel.Range rng = Sheet.Columns.get_Range("A1", "E1");
            rng.EntireColumn.AutoFit();
            App.ActiveWindow.Zoom = 80;
        }

        #endregion //Excel methods

        #region properties ================================================================================

        /// <summary>
        /// Excel filename
        /// </summary>
        public string Filename
        {
            get
            {
                return this.fileName;
            }
            set
            {
                fileName = value;
            }
        }

        /// <summary>
        /// Ampla object fullname
        /// </summary>
        public string Fullname
        {
            get
            {
                return fullName;
            }
            set
            {
                fullName = value;
            }
        }

        /// <summary>
        /// Ampla object to process
        /// </summary>
        public List<AmplaItem> ObjectToProcess
        {
            get
            {
                return objectToProcess;
            }
            set
            {
                objectToProcess = value;
            }
        }

        /// <summary>
        /// Ampla object typename
        /// </summary>
        public string TypeName
        {
            get
            {
                return typeName;
            }
            set
            {
                typeName = value;
            }
        }

        /// <summary>
        /// Is export?
        /// </summary>
        public bool isExport
        {
            get
            {
                return export;
            }
            set
            {
                export = value;
            }
        }

        /// <summary>
        /// Is import?
        /// </summary>
        public bool isImport
        {
            get
            {
                return import;
            }
            set
            {
                import = value;
            }
        }

        /// <summary>
        /// Open Excel configuration file
        /// </summary>
        /// <param name="sFileName">The full path name of the excel config file</param>
        /// <returns>Success or failure</returns>
        public bool isRecursive
        {
            get
            {
                return recursive;
            }
            set
            {
                recursive = value;
            }
        }
        #endregion ========================================================================================

        #region Memory data objects =======================================================================

        private void CreateOutput()
        {
            outputDataSet = new DataSet();
            DataTable outputTable = new DataTable("Data");
            outputTable.Columns.Add(new DataColumn("TypeName", typeof(string)));
            outputTable.Columns.Add(new DataColumn("FullName", typeof(string)));
            outputTable.Columns.Add(new DataColumn("PropertyName", typeof(string)));
            outputTable.Columns.Add(new DataColumn("PropertyValue", typeof(string)));
            outputDataSet.Tables.Add(outputTable);
        }

        private void CreateInput()
        {
            inputDataSet = new DataSet();
            DataTable inputTable = new DataTable("Data");
            inputTable.Columns.Add(new DataColumn("TypeName", typeof(string)));
            inputTable.Columns.Add(new DataColumn("FullName", typeof(string)));
            inputTable.Columns.Add(new DataColumn("PropertyName", typeof(string)));
            inputTable.Columns.Add(new DataColumn("PropertyValue", typeof(string)));
            inputTable.Columns.Add(new DataColumn("Imported", typeof(string)));
            inputDataSet.Tables.Add(inputTable);
        }

        /// <summary>
        /// Adds a new row to output
        /// <param name="fullname"></param>
        /// <param name="propertyname"></param>
        /// <param name="propertyvalue"></param>
        private void AddOutputRow(string typename, string fullname, string propertyname, object propertyvalue)
        {
            count = row - 1;
            //setCellValue(row, 1, typename);
            //setCellValue(row, 2, fullname);
            //setCellValue(row, 3, propertyname);
            //setCellValue(row, 4, propertyvalue);
            DataRow outputRow = outputDataSet.Tables["Data"].NewRow();
            outputRow["TypeName"] = typename;
            outputRow["FullName"] = fullname;
            outputRow["PropertyName"] = propertyname;
            outputRow["PropertyValue"] = (propertyvalue != null) ? propertyvalue.ToString() : string.Empty;
            outputDataSet.Tables["Data"].Rows.Add(outputRow);

            //if (propertyname == string.Empty) FormatItemRow(row);
            options.UpdateStatus(string.Concat("Fullname: ", fullname, " Property: ", propertyname));
            //if (propertyname != string.Empty) mainLog.WriteLine(string.Concat("Property: ", propertyname));
            //if (propertyvalue != null) mainLog.WriteLine(string.Concat("Value: ", propertyvalue));
            row++;
        }
        /// <summary>
        /// 
        /// </summary>
        private void ExportOutput()
        {
            try
            {
                int outputRowIndex;
                int dataIndex = 0;
                options.UpdateStatus("Exporting DataSet to Excel..................");

                DataTable data = outputDataSet.Tables["Data"];
                int blockSize = (int.TryParse(GetKeyValue("ExcelBatchSize"), out blockSize)) ? blockSize : 60000;

                int numberOfBlocks = (data.Rows.Count % blockSize == 0) ? (data.Rows.Count / blockSize) : (data.Rows.Count / blockSize + 1);

                for (int i = 0; i < numberOfBlocks; i++)
                {
                    ArrayList longCells = new ArrayList();
                    ArrayList rowNum = new ArrayList();
                    ArrayList colNum = new ArrayList();

                    int arraySize = (numberOfBlocks == (i + 1)) ? (data.Rows.Count - (numberOfBlocks -1) * blockSize) : blockSize;
                    string[,] cellValues = new string[arraySize, 4];
                    int rowIndex = (i == 0) ? 2 : i * blockSize + 2;

                    if ((dataIndex >= i * blockSize) && (dataIndex < (i + 1) * blockSize) && i != 0)
                    {
                        string name = string.Format("{0}{1}", exportSheet, i.ToString("00"));
                        Workbook.Sheets.Add(Missing.Value, Missing.Value, Missing.Value, Missing.Value);
                        Excel.Worksheet sheet = Workbook.ActiveSheet as Excel.Worksheet;
                        sheet.Name = name;
                        sheet.Move(Missing.Value,Workbook.Sheets[Workbook.Sheets.Count]);
                        this.Sheet = sheet;
                        mainLog.WriteLine(string.Format("Writing Excel sheet {0}..", name));
                        populateColumnHeader();
                        rowIndex = 2;
                    }

                    for (outputRowIndex = rowIndex; outputRowIndex < rowIndex + arraySize; outputRowIndex++)
                    {
                        cellValues[outputRowIndex - rowIndex, 0] = data.Rows[dataIndex]["TypeName"].ToString();
                        cellValues[outputRowIndex - rowIndex, 1] = data.Rows[dataIndex]["FullName"].ToString();
                        cellValues[outputRowIndex - rowIndex, 2] = data.Rows[dataIndex]["PropertyName"].ToString();
                        cellValues[outputRowIndex - rowIndex, 3] = data.Rows[dataIndex]["PropertyValue"].ToString();
                        for (int index = 0; index < 4; index++)
                        {
                            if (cellValues[outputRowIndex - rowIndex, index].Length > 911)
                            {   //When setting a range of values, the maximum length of a single cell is 911 characters.
                                longCells.Add(cellValues[outputRowIndex - rowIndex, index]);
                                rowNum.Add(outputRowIndex);
                                colNum.Add(index + 1);
                                cellValues[outputRowIndex - rowIndex, index] = "THIS CELL WAS LONGER THAN 911 CHARACTERS!";
                            }
                        }

                        dataIndex++;
                    }

                    Excel.Range cells = (Excel.Range)Sheet.get_Range(string.Format("A{0}:D{1}", rowIndex, outputRowIndex-1), Type.Missing);
                    cells.Value2 = cellValues;

                    //Add long cells individually:
                    int k = 0;
                    foreach (string s in longCells)
                    {
                        setCellValue((int)rowNum[k], (int)colNum[k], s);
                        k++;
                    }
                    FormatTopRow();
                    FormatAutoFit();
                }
            }
            catch (Exception ex)
            {
                mainLog.RaiseException(ex);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        private void ImportInput()
        {
            int inputRowIndex = 2;
            DataRow dr;

            options.UpdateStatus("Importing Excel file into memory..................");

            for (int i = 1; i <= Workbook.Sheets.Count; i++ )
            {
                Sheet = Workbook.Worksheets[i] as Excel.Worksheet;
                string test = Sheet.Name;
                if (ValidateSheet(Sheet))
                {
                    inputRowIndex = 2;
                    Sheet.Select(Missing.Value);
                    while (isRowOK(inputRowIndex))
                    {
                        dr = inputDataSet.Tables["Data"].NewRow();
                        dr["TypeName"] = getCellString(inputRowIndex, colTypeName);
                        dr["FullName"] = getCellString(inputRowIndex, colFullName);
                        dr["PropertyName"] = getCellString(inputRowIndex, colPropertyName);
                        dr["PropertyValue"] = getCellString(inputRowIndex, colPropertyValue);
                        if (isRowEdited(inputRowIndex)) dr["Imported"] = import;
                        inputDataSet.Tables["Data"].Rows.Add(dr);
                        inputRowIndex++;
                        options.UpdateStatus(string.Format("Importing into memory record: '{0}' Sheet: '{1}' of '{2}'",inputRowIndex.ToString(),i.ToString(),Workbook.Sheets.Count.ToString()));
                    }

                }
            }
            options.UpdateStatus("Number of Excel rows to process: " + inputRowIndex.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        private void CreateXmlImportResultsFile()
        {
            string outputFilename = string.Concat(fileName.Substring(0,fileName.IndexOf(".xml")),"Imported.xlm"); 
            inputDataSet.Tables["Data"].DefaultView.RowFilter = string.Format("Imported <> '{0}'", imported);
            DataTable outputResults = inputDataSet.Tables["Data"].DefaultView.ToTable();
            options.UpdateStatus(string.Format("Creating results output xml file : '{0}'", outputFilename));
            outputResults.WriteXml(outputFilename);
        }

        /// <summary>
        /// 
        /// </summary>
        private void UpdateImportedExcel()
        {
            int outputRowIndex = 2;
            int dataIndex = 0;
            int numberOfRows = inputDataSet.Tables["Data"].Rows.Count;

            foreach (Excel.Worksheet sheet in Workbook.Sheets)
            {
                Sheet = GetSheet(sheet.Name);
                if (!ValidateSheet(Sheet)) continue;
                outputRowIndex = 2;
                Sheet = sheet;
                string test = Sheet.Name;
                while (getCellValue(outputRowIndex,1) != null)
                {
                    DataRow dr = inputDataSet.Tables["Data"].Rows[dataIndex];
                    if (dr["Imported"].ToString() == imported)
                    {
                        FormatProcessed(outputRowIndex, 3, 2);
                    }
                    dataIndex++;
                    outputRowIndex++;
                }

            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void ExportOutput1()
        {
            int outputRowIndex = 2;

            options.UpdateStatus("Exporting DataSet to Excel..................");
            try
            {
                OleDbConnection conOleDBConnection = new OleDbConnection();
                OleDbCommand comOleDBCommand = new OleDbCommand();
                conOleDBConnection.ConnectionString = ("Data Source=" + this.fileName + ";" + ("Provider=Microsoft.Jet.OLEDB.4.0;" + "Extended Properties=Excel 8.0;"));
                comOleDBCommand.Connection = conOleDBConnection;
                conOleDBConnection.Open();
                comOleDBCommand.CommandText = "CREATE TABLE [MySheetName] (TypeName string, FullName string, PropertyName string, PropertyValue string);" + "";
                comOleDBCommand.ExecuteNonQuery();
                string typename;
                string fullname;
                string propertyname;
                string propertyvalue;

                foreach (DataRow dr in outputDataSet.Tables["Data"].Rows)
                {
                    typename = (dr["TypeName"] != null) ? dr["TypeName"].ToString() : string.Empty;
                    fullname = (dr["FullName"] != null) ? dr["FullName"].ToString() : string.Empty;
                    propertyname = (dr["PropertyName"] != null) ? dr["PropertyName"].ToString() : string.Empty;
                    propertyvalue = (dr["PropertyValue"] != null) ? dr["PropertyValue"].ToString() : string.Empty;

                    comOleDBCommand.CommandType = CommandType.Text;
                    comOleDBCommand.CommandText = ("INSERT INTO [MySheetName] (TypeName, FullName, PropertyName, PropertyValue) VALUES (\'"
                                + (typename + ("\',\'"
                                + (fullname + ("\',\'"
                                + (propertyname + ("\',\'"
                                + (propertyvalue + "\');"))))))));


                    comOleDBCommand.ExecuteNonQuery();
                }
                outputRowIndex++;
            }
            catch (Exception ex)
            {
                mainLog.RaiseException(ex);
            }
        }

        private bool IsImportRow(DataRow dr)
        {
            if (dr["Imported"].ToString() == singleImport) return true;
            return false;
        }

        private void RecordImported(int rowIndex)
        {
            DataRow dr = inputDataSet.Tables["Data"].Rows[rowIndex];
            dr["Imported"] = imported;
        }

        #endregion //Memory data objects
    }
}
