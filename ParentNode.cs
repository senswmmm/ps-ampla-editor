//--------------------------------------------------------------------------------------------------
//  Copyright (c) 2006 Citect Pty Limited.
//  All rights reserved. 
//	
//	History:
//			23/05/2006		Julio Montufar		Original
//--------------------------------------------------------------------------------------------------

using System;
using System.Windows.Forms;
using System.Collections.Generic;

#region Citect/Ampla references------------------------------------------------------------------------------

using Citect.Ampla.Framework;
using Citect.Ampla.Framework.ClassSystem;

#endregion //Ampla referencesz

namespace PSAmplaEditor
{
    public class ParentNode : System.Windows.Forms.TreeNode
    {
		#region fields---------------------------------------------------------------------------------------
		
		public bool childrenAdded;
        private Type type;
        private string name;
        private Guid id;
        private string fullname;
        private List<ClassDefinition> definitions;

		#endregion //fields

		#region constructors---------------------------------------------------------------------------------

        public ParentNode(String text): base(text) 
        {
  
        }

		#endregion //constructor

		#region properties-----------------------------------------------------------------------------------

		/// <summary>
		/// Children nodes added
		/// </summary>
        public bool ChildrenAdded
        {
            set
            {
                childrenAdded = value;
            }
            get
            {
                return childrenAdded;
            }
        }

		/// <summary>
		/// Node fullname
		/// </summary>
        public string Fullname
        {
            set
            {
                fullname = value.ToString();
            }
            get
            {
                return fullname;
            }
        }

		/// <summary>
		/// Node name
		/// </summary>
        public string Name
        {
            set
            {
                name = value.ToString();
            }
            get
            {
                return name;
            }
        }

        /// <summary>
        /// Node fullname
        /// </summary>
        public Type ObjectType
        {
            set
            {
                type = value;
            }
            get
            {
                return type;
            }
        }

		/// <summary>
		/// Node guid
		/// </summary>
        public Guid Id
        {
            set
            {
                id = value;
            }
            get
            {
                return id;
            }
        }

        /// <summary>
        /// Node guid
        /// </summary>
        public List<ClassDefinition> SubClassDefinitions
        {
            set
            {
                definitions = value;
            }
            get
            {
                return definitions;
            }
        }

		/// <summary>
		/// Custom implementation used in ListBox
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			return fullname;
		}

		#endregion //properties
    }
}
