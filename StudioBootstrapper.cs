namespace PSAmplaEditor
{
    using Autofac;
    using Autofac.Builder;
    using Citect.Ampla.Framework.DependencyInjection;
    using Citect.Common;
    using Citect.Common.EventBrokering;
    using Citect.Common.Gui;
    using System;
    using System.Reflection;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Threading;
    using Citect.Ampla.Studio.SandDock.Gui;
    using Citect.Ampla.Framework;
    using Autofac.Features.OwnedInstances;
    using Citect.Ampla.Framework.Gui;
    using Citect.Ampla.DependencyInjection;


    public sealed class StudioBootstrapper : Citect.Common.Gui.Bootstrapper<Citect.Ampla.Studio.SandDock.Gui.MainForm>, IDisposable
    {
        private readonly string[] arguments;
        


        public StudioBootstrapper() : this(Environment.GetCommandLineArgs())
        {
        }

        public StudioBootstrapper(string[] commandLineArguments)
        {
            this.arguments = commandLineArguments;
        }
        public void RegisterAssemblies(ContainerBuilder builder)
        {
            Assembly[] assemblies = this.GetModuleAssemblies().ToArray<Assembly>();
            builder.RegisterAssemblyModules<UIModule>(assemblies);
        }

        protected override Citect.Ampla.Studio.SandDock.Gui.MainForm CreateShell()
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en");
            return base.Container.Resolve<Citect.Ampla.Studio.SandDock.Gui.MainForm>();
        }

 


        public void ConfiguringContainer(ContainerBuilder builder)
        {
            RegisterAssemblies(builder);
            builder.RegisterModule(new ProjectModule());
            builder.Register<EventBroker>(((Func<IComponentContext, EventBroker>) (c => new EventBroker()))).As<IEventBroker>().SingleInstance();
            builder.Register<Citect.Ampla.Studio.SandDock.Gui.MainForm>(((Func<IComponentContext, Citect.Ampla.Studio.SandDock.Gui.MainForm>)(c => new Citect.Ampla.Studio.SandDock.Gui.MainForm(this.arguments, c.Resolve<ILifetimeScope>())))).AsSelf<Citect.Ampla.Studio.SandDock.Gui.MainForm, SimpleActivatorData>().As<IViewInstrumentationProvider>().PropertiesAutowired(PropertyWiringFlags.Default).SingleInstance();
            this.Container = builder.Build(ContainerBuildOptions.Default);
        }

        public ILifetimeScope GetLifetimeScope()
        {
             
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en");
            Citect.Ampla.Studio.SandDock.Gui.MainForm form = this.Container.Resolve<Citect.Ampla.Studio.SandDock.Gui.MainForm>();
            return this.Container.BeginItemLifetimeScope();

        }

        protected override IEnumerable<Assembly> GetModuleAssemblies()
        {
            return base.GetModuleAssemblies().Union<Assembly>(InstalledReferences.GetAssemblies());
        }

        void IDisposable.Dispose()
        {
            base.Dispose();
        }
        public IContainer Container { get; private set; }
    }
}
