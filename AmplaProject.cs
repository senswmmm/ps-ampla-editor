//---------------------------------------------------------------------------------------------------------------------
//  Copyright (c) 2006 Citect Pty Limited.
//  All rights reserved. 
//	
//	History:
//			23/05/2006		Julio Montufar		Original
//---------------------------------------------------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections;
using System.IO;
using System.Security.Principal;
using System.Diagnostics;
using System.Windows.Forms;

#region Citect/Ampla references-----------------------------------------------------------------------------------------

using Citect.Ampla.Framework;
using Citect.Ampla.Server;
using Citect.Common.Gui;
using Citect.Ampla.Framework.Gui;
using System.Security.Policy;
using Citect.Ampla.Runtime.Remoting.Scripting;
using Autofac.Core;
using Autofac.Features.OwnedInstances;

#endregion //Ampla refereneces

namespace PSAmplaEditor
{
	/// <summary>
	/// Gets Ampla project.
	/// </summary>
	public class AmplaProject
	{
		
		#region fields--------------------------------------------------------------------------------------------------

		private string userSettingsPath;/// <summary>
		private string projectname = string.Empty;
		private AppForm mainform;
        private readonly Func<Uri, Project.ProgressUpdate, Owned<ClientProject>> clientProjectFactory;

		#endregion //fields

		#region constructor---------------------------------------------------------------------------------------------

        public AmplaProject(AppForm form,Func<Uri, Project.ProgressUpdate, Owned<ClientProject>> clientProjectFactory)
		{
            this.mainform = form;
            this.clientProjectFactory = clientProjectFactory;

			//
			// TODO: Add constructor logic here
			//
		}
		public AmplaProject()
		{
		}
		#endregion //constructor

		#region methods-------------------------------------------------------------------------------------------------

        private void projectProgressUpdate(string message, int progress)
        {
           // this.setStatusBarLabelText(message);
           // this.stepProgressBar((int)(0.8 * progress));
        }

		/// <summary>
		/// Gets Ampla project
		/// </summary>
		/// <returns></returns>
		public ClientProject GetProject()
		{
			const int maximumConnectionHistory = 10;
			try
			{
				ComboHistory connectionHistory = ComboHistory.Load(Path.Combine(UserSettingsPath, "ConnectHistory.xml"));
				if (connectionHistory == null)
				{
					connectionHistory = new ComboHistory(maximumConnectionHistory);
				}
				Citect.Ampla.Framework.Gui.ServerConnectForm serverConnectForm = new Citect.Ampla.Framework.Gui.ServerConnectForm();
				serverConnectForm.LoadHistory(connectionHistory);
				serverConnectForm.TopMost = true;
				if (serverConnectForm.ShowDialog(new AppForm()) == DialogResult.OK)
				{
					mainform.Invoke(new SetNotificationText(mainform.SetNotification), new object[] { string.Empty });
					mainform.Invoke(new Loading(mainform.ShowLoading), new object[] { true });
                    Owned<ClientProject> newProject = this.clientProjectFactory(serverConnectForm.ServerUri, new Project.ProgressUpdate(this.projectProgressUpdate));
                    ClientProject project = newProject.Value;
					if (project != null) 
					{
						projectname = serverConnectForm.ServerName;
						connectionHistory.AddEntry(serverConnectForm.ServerName);
						connectionHistory.Save(Path.Combine(UserSettingsPath, "ConnectHistory.xml"));
						return project;
					}
				}
				return null;
				
			}	
			catch (PolicyException ex)
			{
				Console.WriteLine(string.Concat(ex,
					"Security permissions have prevented the BulkEditTool from loading some references. Consider using" + Environment.NewLine +
					"the BulkEditToolUtil to open up permissions to 'C:\\Windows\\assembly' or its equivalent"));
				return null;
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
				return null;
			}
		}

		/// <summary>
		/// Gets or sets the UserSettingsPath.
		/// </summary>
		private string UserSettingsPath
		{
			get
			{
				if (userSettingsPath == null)
				{
					userSettingsPath = Path.Combine(Citect.Common.AmplaEnvironment.UserApplicationData, "Studio");
				}
				if (!Directory.Exists(userSettingsPath))
				{
					Directory.CreateDirectory(userSettingsPath);
				}
				return userSettingsPath;
			}
		}
		
		public bool IsCompiled(ClientProject project, Log mainLog)
		{
			string errors = string.Empty;
            try
            {
                bool validate = project.Validate();
                CodeCompileResults compileResults = project.ValidateScript();
                foreach (CodeCompileError error in compileResults.Errors)
                {
                    errors = errors + error.ErrorText + "\n";
                }
                if (errors != string.Empty)
                {

                    mainLog.WriteLine(errors);
                    MessageBox.Show(errors, "Failed to save project");
                }
                return (compileResults.Errors.Count == 0);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
			
		}
		/// <summary>
		/// Get Amppla Server name
		/// </summary>
		public string ProjectName
		{
			get
			{
				return projectname;
			}
		}
		#endregion ============================================================================================
		
	}
}
