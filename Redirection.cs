//-----------------------------------------------------------------------------------------------
//  Copyright (c) 2006 Citect Pty Limited.
//  All rights reserved. 
//	
//	History:
//			11/11/2008		Julio Montufar		Original
//	Version 3.2XXX
//-----------------------------------------------------------------------------------------------

#region References --------------------------------------------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.IO;
using System.Reflection;
using System.Xml;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Configuration;
using System.Windows.Forms;

#endregion //References


namespace PSAmplaEditor
{
    class Redirection
    {

        #region Constants----------------------------------------------------------------------------------------------------------------------------

        private const string constRedirectAssemblies = "RedirectAssemblies";
        private const string constBuildAssemblies = "BuildAssemblies";
        private const string consRuntime = "runtime";
        private const string constAssemblyBinding = "assemblyBinding";
        private const string constDependentAssembly = "dependentAssembly";
        private const string constAssemblyIdentity = "assemblyIdentity";
        private const string constBindingRedirect = "bindingRedirect";
        private const string constOldVersion = "oldVersion";
        private const string constNewVerssion = "newVersion";

        #endregion //Constants

        #region fields-------------------------------------------------------------------------------------------------------------------------------

        private Log mainLog;

        #endregion //fields

        #region constructors-------------------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="log"></param>
        public Redirection(Log log)
        {
            this.mainLog = log;
        }

        #endregion

        #region Methods -----------------------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Creates assembly redirection section under configuration/runtime/assemblybinding
        /// </summary>
        /// <returns></returns>
        public bool AddRedirection()
        {
            try
            {
                string aConfigPath = Application.ExecutablePath + ".config";
                string redirectAssemblies = GetKeyValue(constRedirectAssemblies);

                if (redirectAssemblies != string.Empty && Convert.ToBoolean(redirectAssemblies) && File.Exists(aConfigPath))
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(aConfigPath);
                    XmlNode xmlNode = xmlDoc.DocumentElement.SelectSingleNode(consRuntime);
                    if (xmlNode != null)
                    {
                        XmlNode assemblyBinding = GetNode(constAssemblyBinding, xmlNode.ChildNodes);
                        removeRedirectedAssemblies(xmlDoc, assemblyBinding);

                        if (assemblyBinding != null && !NodeFound(constDependentAssembly, assemblyBinding.ChildNodes))
                        {
                            Assembly a = Assembly.GetExecutingAssembly();
                            foreach (AssemblyName an in a.GetReferencedAssemblies())
                            {
                                string dll = an.Name;
                                if (dll.Contains("Citect"))
                                {
                                    XmlNode dependentAssembly = AddElement(constDependentAssembly, null, assemblyBinding);
                                    XmlNode assemblyIdentity = AddElement(constAssemblyIdentity, null, dependentAssembly);
                                    AddAttribute("name", dll, assemblyIdentity);
                                    string token = GetBuildAmplaVersion(dll, "PublicKeyToken");
                                    AddAttribute("publicKeyToken", token, assemblyIdentity);
                                    XmlNode bindingRedirect = AddElement(constBindingRedirect, null, dependentAssembly);
                                    AddAttribute(constOldVersion, GetBuildAmplaVersion(dll, "Version"), bindingRedirect);
                                    AddAttribute(constNewVerssion, GetCurrentAmplaVersion(dll + ".dll"), bindingRedirect);
                                    AddAttribute("culture", "", assemblyIdentity);
                                    //AddAttribute("culture", GetBuildAmplaVersion(propArr[i], "Culture"), assemblyIdentity);
                                }

                            }
                            
                            xmlDoc = stripDocumentNamespace(xmlDoc);
                            xmlDoc.Save(aConfigPath);
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                mainLog.WriteLine(ex.Message);
                return false;
            }

        }

        /// <summary>
        /// Get rid of xmlns in XmlDoc
        /// </summary>
        /// <param name="oldDom"></param>
        /// <returns></returns>
        private XmlDocument stripDocumentNamespace(XmlDocument oldDom)
        {
            // Remove all xmlns:* instances from the passed XmlDocument
            // to simplify our xpath expressions.
            XmlDocument newDom = new XmlDocument();
            newDom.LoadXml(oldDom.OuterXml.Replace("xmlns=\"\"", ""));
            return newDom;
        } 

        /// <summary>
        /// Add node element
        /// </summary>
        /// <param name="tagName"></param>
        /// <param name="textContent"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        private XmlNode AddElement(string tagName, string textContent, XmlNode parent)
        {

            XmlNode node = parent.OwnerDocument.CreateElement(tagName);
            parent.AppendChild(node);

            if (textContent != null)
            {

                XmlNode content;
                content = parent.OwnerDocument.CreateTextNode(textContent);
                node.AppendChild(content);
            }
            return node;
        }

        /// <summary>
        /// Add attribute to element
        /// </summary>
        /// <param name="attributeName"></param>
        /// <param name="textContent"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        private XmlNode AddAttribute(string attributeName, string textContent, XmlNode parent)
        {

            XmlAttribute attribute;
            attribute = parent.OwnerDocument.CreateAttribute(attributeName);
            attribute.Value = textContent;
            parent.Attributes.Append(attribute);

            return attribute;
        }

        /// <summary>
        /// Remove section of redirected assemblies from config file
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="assemblyBinding"></param>
        private void removeRedirectedAssemblies(XmlDocument xmlDoc,XmlNode assemblyBinding)
        {
            string aConfigPath = Application.ExecutablePath + ".config";
            string redirectAssemblies = GetKeyValue(constRedirectAssemblies);
            if (assemblyBinding != null && NodeFound(constDependentAssembly, assemblyBinding.ChildNodes))
            {
                List<XmlNode> nodeList = new List<XmlNode>();
                foreach (XmlNode child in assemblyBinding.ChildNodes)
                {
                    if (child.Name == constDependentAssembly) nodeList.Add(child);
                }
                foreach (XmlNode node in nodeList) assemblyBinding.RemoveChild(node);
                xmlDoc.Save(aConfigPath);
            }

        }

        /// <summary>
        /// Search in child nodes for a node
        /// </summary>
        /// <param name="name"></param>
        /// <param name="list"></param>
        /// <returns></returns>
        private XmlNode GetNode(string name, XmlNodeList list )
        {
            foreach (XmlNode node in list)
            {
                if (node.Name == name) return node;
            }
            return null;
        }

        /// <summary>
        /// Returns  true if node found
        /// </summary>
        /// <param name="name"></param>
        /// <param name="list"></param>
        /// <returns></returns>
        private bool NodeFound(string name, XmlNodeList list)
        {
            foreach (XmlNode node in list)
            {
                if (node.Name == name) return true;
                XmlNode childNode = GetNode(name, node.ChildNodes);
            }
            return false;
        }

        /// <summary>
        /// Find specific value in value string
        /// </summary>
        /// <param name="key"></param>
        /// <param name="svalue"></param>
        /// <returns></returns>
        private bool IsContained(string key, string svalue)
        {
            System.Configuration.AppSettingsReader reader = new System.Configuration.AppSettingsReader();
            String[] propArr = reader.GetValue(key, typeof(System.String)).ToString().Split(new Char[] { ',' });
            for (int i = 0; i < propArr.Length; i++)
            {
                if (svalue == propArr[i].ToString()) return true;
            }
            return false;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private string GetKeyValue(string key)
        {
            System.Configuration.AppSettingsReader reader = new System.Configuration.AppSettingsReader();
            return reader.GetValue(key, typeof(System.String)).ToString();
        }

        /// <summary>
        /// Validate open excel file
        /// </summary>
        /// <returns>Success or failure</returns>
        private string GetAmplaDirectory()
        {
            AppSettingsReader reader = new AppSettingsReader();
            string amplaDirectory = reader.GetValue("AmplaDirectory", typeof(System.String)).ToString();
            if (Directory.Exists(@amplaDirectory))
            {
                return @amplaDirectory;
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets Ampla version from Ampla dll
        /// </summary>
        /// <returns></returns>
        private string GetCurrentAmplaVersion(string dll)
        {
            string AmplaDirectory = GetAmplaDirectory();
            DirectoryInfo dir = new DirectoryInfo(AmplaDirectory);
            FileInfo[] dllfiles = dir.GetFiles("*.dll");
            foreach (FileInfo f in dllfiles)
            {
                if (f.Name == dll)
                {
                    return FileVersionInfo.GetVersionInfo(f.FullName).FileVersion.ToString();
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// Gets the buid number of Ampla for this product
        /// </summary>
        /// <returns></returns>
        private string GetBuildAmplaVersion(string dll, string name)
        {
            Assembly a = Assembly.GetExecutingAssembly();
            foreach (AssemblyName an in a.GetReferencedAssemblies())
            {
                if (an.Name == dll)
                {
                    if (name == "Version") return an.Version.ToString();
                    if (name == "PublicKeyToken") return PublicKeyToken(an);
                    if (name == "Culture") return an.CultureInfo.ToString();
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// Gets the PublicKeyToken as string
        /// </summary>
        /// <param name="an"></param>
        /// <returns></returns>
        private string PublicKeyToken(AssemblyName an)
        {
            const byte mask = 15;
            const string hex = "0123456789ABCDEF";

            StringBuilder pkt = new StringBuilder();
            foreach (byte b in an.GetPublicKeyToken())
            {
                pkt.Append(hex[b / 16 & mask]);
                pkt.Append(hex[b & mask]);
            }
           return pkt.ToString();
        }
        #endregion //Methods

    }
}
